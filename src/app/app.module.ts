import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { SharedModule } from "./shared/shared.module";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { FormsModule } from "@angular/forms";
import { AgmCoreModule, GoogleMapsAPIWrapper, MarkerManager } from '@agm/core';

// declartion
import { AppComponent } from "./app.component";
import { LoginComponent } from "./auth/login/login.component";
// providers
import { UserService } from "./pages/user/service/user.service";
import { WebServicesService } from "./providers/web-services.service";
import { HandleErrorService } from "./providers/handle-error.service";
import { LoginService } from "./auth/services/login.service";

import { RootService } from './pages/root/service/root.service';
import { AddRootService } from './pages/root/service/add-root.service';
import { UserTerritoryService } from './pages/root/service/user-territory.service';
import { PlanService } from './pages/plan/service/plan.service';
import { CustomerService } from './pages/customer/service/customer.service';
import { AddEditCustomerService } from './pages/customer/service/add-edit-customer.service';



@NgModule({
  declarations: [AppComponent, LoginComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyAgAtnMefRdt45U6mw5Jbv8aM1K8CNvBt4",
      libraries: ["places", 'geometry']
    }),

  ],
  providers: [
    WebServicesService,
    LoginService,
    HandleErrorService,
    UserService,
    RootService,
    AddRootService,
    UserTerritoryService,
    PlanService,
    GoogleMapsAPIWrapper,
    MarkerManager,
    CustomerService,
    AddEditCustomerService,    
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
