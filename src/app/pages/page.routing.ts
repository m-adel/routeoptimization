import { HomeComponent } from "./home/home.component";
import { PageComponent } from "./page/page.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ProfileComponent } from "./profile/profile.component";

const routes: Routes = [
  {
    path: "",
    component: PageComponent,
    children: [
      {
        path: "customer",
        loadChildren: "./customer/customer.module#CustomerModule"
      },
      {
        path: "home",
        component: HomeComponent
      },
      {
        path: "",
        redirectTo: "/home",
        pathMatch: "full"
      },
      {
        path: "user",
        loadChildren: "./user/user.module#UserModule"
      },
      {
        path: "root",
        loadChildren: "./root/root.module#RootModule"
      },
      {
        path: "plan",
        loadChildren: "./plan/plan.module#PlanModule"
      },
  
      {
        path: "profile",
        component: ProfileComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule {}
