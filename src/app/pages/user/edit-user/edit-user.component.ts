import { UserService } from '../service/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.sass']
})
export class EditUserComponent implements OnInit {

  constructor(public router: Router, public activeRoute: ActivatedRoute, private userProvider: UserService) { }
  id: any;
  loaded = true;
  user = {
    firstName: "",
    lastName: "",
    password: "",
    phoneNumber: "",
    userName: "",
    confirmPassword: "",
    profile: {
      profile: 0,
      profileCode: "",
      profileDescription: ""
    }
  };
  success = false;
  error = false;
  Responsemessage = false;
  ngOnInit() {
    this.id = this.activeRoute.snapshot.params.id;
    this.showUser();
  }
  showUser() {
    this.userProvider.showProfile(this.id).subscribe((Res: any) => {
      this.user = Res;
      this.user.confirmPassword = this.user.password;
      this.loaded = false;
    });
  }
  handleBackToGetAllUser() {
    this.router.navigateByUrl("/pages/user/all-users");
  }
}
