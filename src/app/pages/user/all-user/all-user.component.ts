import { HandleErrorService } from '../../../providers/handle-error.service';
import { UserService } from '../service/user.service';
import { distinctUntilChanged } from 'rxjs/operators';
import { debounceTime, map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-all-user',
  templateUrl: './all-user.component.html',
  styleUrls: ['./all-user.component.sass']
})
export class AllUserComponent implements OnInit {
  data = [];
  originalData = [];
  page: any = 1;
  pageSize: any = 10;
  totalItems: any = 0;
  offset = 0;
  Responsemessage = "";
  error = false;
  seaching = false;
  loaded = true;
  userId: any;
  indexOfUserId: any;
  constructor(
    public router: Router,
    private userProvider: UserService,
    private handleError: HandleErrorService
  ) { }

  ngOnInit() {
    this.getCountOfUsers();
  }
  getCountOfUsers() {
    this.userProvider.getCountOfUser().subscribe((Res: any) => {
      this.totalItems = Res;
      this.getAllUser();
    }, err => {
      this.Responsemessage = this.handleError.errorHandler(err.status, "pages");
      this.error = true;
      setTimeout(() => {
        this.error = false;
        this.router.navigate(["/login"]);
      }, 2000);
    });
  }
  getAllUser() {
    this.userProvider.getAllUser(this.offset, this.pageSize).subscribe((Res: any) => {
      this.data = Res.filter(v => v.userId != sessionStorage.getItem("userId"));
      this.originalData = this.data;
      this.loaded = false;
      this.seaching = false;

    }, err => {
      this.Responsemessage = this.handleError.errorHandler(err.status, "pages");
      this.error = true;
      setTimeout(() => this.error = false, 2000);
    });
  }
  PageChange(e) {
    this.loaded = true;
    this.page = e - 1;
    this.offset = this.page * this.pageSize;
    this.ngOnInit();
  }
  onPressEditIcon(id) {
    this.router.navigate(["/pages/user/edit-user", id]);
  }

  onChange(e) {
    this.offset = 0;
    const UserName = e.target.value;
    if (UserName == "") {
      this.seaching = false;
      this.ngOnInit();
    } else {
      this.userProvider.searchByUserName(UserName, this.offset, this.totalItems).subscribe((Res: any) => {
          this.data = Res;
          this.seaching = true;
          if (UserName == "") {
            this.seaching = false;
            this.ngOnInit();
          }
      });
    }
  }

  onPressDeleteIcon(user, index) {
    this.userId = user;
    this.indexOfUserId = index;
  }
  handelDeleteUser() {
    this.userProvider.deletedUser(this.userId).subscribe((Res: any) => {
      this.data.splice(this.indexOfUserId, 1);
    }, err => {
      console.log(err);
      if (err.error.text === "Entity deleted successfully") {
        this.data.splice(this.indexOfUserId, 1);
        this.Responsemessage = "Deleted Successfully";
        this.error = true;
        setTimeout(() => this.error = false, 2000);
      } else {
        this.Responsemessage = this.handleError.errorHandler(err.status, "pages");
        this.error = true;
        setTimeout(() => this.error = false, 2000);
      }
    });
  }

}
