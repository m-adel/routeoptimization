import { WebServicesService } from '../../../providers/web-services.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class UserService {

  constructor(public http: HttpClient, public config: WebServicesService) { }
  getCountOfUser() {
    const url = this.config.serviceUrl + this.config.getCountOfUsers.replace("{2}", sessionStorage.getItem("companyCode"));
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getAllUser(Offset, limit) {
    const url = this.config.serviceUrl + this.config.getAllUserInCompany.
    replace("{0}", sessionStorage.getItem("companyCode")).replace("{1}", Offset).
    replace("{2}", limit);
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }

  getAllUsers(){
    const url = this.config.serviceUrl + "users"
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }

  getAllUsersForAddingTerritory(Offset, limit) {
    const url = this.config.serviceUrl + this.config.getAllUsersInCompanyTerritory.
    replace("{0}", sessionStorage.getItem("companyCode")).replace("{1}", Offset).
    replace("{2}", limit).
    replace("{3}", "1");
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }

  searchByUserName(userName, offset, limit) {
    const url = this.config.serviceUrl + this.config.searchByUserNameInCompany.
    replace("{0}", userName).replace("{1}", sessionStorage.getItem("companyCode"))
    .replace("{2}", limit).replace("{3}", offset);
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getAllProfil() {
    const url = this.config.serviceUrl + this.config.getProfiles;
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  createNewUser(data) {
    const url = this.config.serviceUrl + this.config.createNewUser.replace("{companyCode}", sessionStorage.getItem("companyCode"));
    const header = this.config.headers;
    return this.http.post(url, data, {headers: header});
  }
  deletedUser(id) {
   const url = this.config.serviceUrl + this.config.deleteUser.replace("{userCode}", id);
   const header = this.config.headers;
   return this.http.delete(url, {headers: header});
  }
  showProfile(id) {
    const url = this.config.serviceUrl + this.config.viewProfile.replace("{userId}", id);
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
}
