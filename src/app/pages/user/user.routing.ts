import { AllUserComponent } from './all-user/all-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { AddUserComponent } from './add-user/add-user.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from "./user.component";

const routes: Routes = [
    {
        path: '', component: UserComponent, children: [
            {
                path: '',
                redirectTo: '/pages/user/all-users',
                pathMatch: 'full'
                // component: AllUserComponent
            },
            {
                path: 'add-user', component: AddUserComponent
            },
            {
                path: "edit-user/:id", component: EditUserComponent
            },
            {
                path: "all-users", component: AllUserComponent
            },
        ]
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule { }
