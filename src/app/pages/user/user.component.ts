import { Component } from '@angular/core';

@Component({
  selector: 'app-user',
  template: `
  <!-- <app-child-navs [navs]="navs"></app-child-navs> -->

  <router-outlet></router-outlet>
  `
})
export class UserComponent {
  navs = [
    { name: "All Users", link: "/pages/user/all-users" },
    { name: "Add User", link: "/pages/user/add-user" },
  ];
}
