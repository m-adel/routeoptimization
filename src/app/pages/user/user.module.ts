import { HttpClientModule } from '@angular/common/http';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap/typeahead/typeahead.module';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { AllUserComponent } from './all-user/all-user.component';
import { UserComponent } from "./user.component";
import { UserRoutingModule } from "./user.routing";
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [AddUserComponent, EditUserComponent, AllUserComponent, UserComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    SharedModule,
    HttpClientModule
  ],
})
export class UserModule { }
