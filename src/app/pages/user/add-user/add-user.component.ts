import { HandleErrorService } from "../../../providers/handle-error.service";
import { UserService } from "../service/user.service";
import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.sass"],
})
export class AddUserComponent implements OnInit {
  user = {
    email: "",
    firstName: "",
    lastName: "",
    password: "",
    phoneNumber: "",
    userName: "",
    confirmPassword: "",
    profile: {
      profile: 0,
      profileCode: "",
      profileDescription: "",
    },
  };
  allUsers: any = [];
  mustMatch = false;
  profiles = [];
  loaded = true;
  success = false;
  Responsemessage;
  error = false;
  userNameError;
  phonePattren = true;
  userNameChecked: boolean;
  constructor(
    public router: Router,
    private userProvider: UserService,
    private errorHandle: HandleErrorService
  ) {}

  ngOnInit() {
    this.getAllProfiles();
    this.getAllUsers();
  }
  getAllProfiles() {
    this.userProvider.getAllProfil().subscribe((Res: any) => {
      this.profiles = Res;
      console.log(this.profiles);

      this.loaded = false;
    });
  }
  getAllUsers() {
    this.userProvider.getAllUsers().subscribe((res) => {
      this.allUsers = res;
      console.log(this.allUsers);
    });
  }

  onChangeCurrentPassword(e) {
    this.mustMatch =
      this.user.confirmPassword === this.user.password ? false : true;
  }
  onSelectProfile(event) {
    this.profiles.forEach((el) =>
      this.user.profile.profileCode === el.profileCode
        ? (this.user.profile = el)
        : ""
    );
  }

  userNameCheck() {
    for (let i = 0; i < this.allUsers.length; i++) {
      if (this.user.userName === this.allUsers[i].userName) {
        this.userNameChecked = false;

        break;

      } else {
        this.userNameChecked =true;
      }
    }
    console.log(this.userNameChecked);

    return this.userNameChecked;
  }

  handleAddUser() {
    if (this.userNameCheck()==true) {
      this.userProvider.createNewUser(this.user).subscribe(
        (Res: any) => {
          this.Responsemessage = "Creating Successfully !";
          this.success = true;
          setTimeout(() => {
            this.success = false;
            this.router.navigateByUrl("/pages/user/all-users");
          }, 2000);
        },
        (err) => {
          this.error = true;
          this.Responsemessage = this.errorHandle.errorHandler(
            err.status,
            "pages"
          );
        }
      );
    }else{
      this.error=true
      this.userNameError= "this user name is already used"

    }
  }
  handleBackToAllUser() {
    this.router.navigateByUrl("/pages/user/all-users");
  }
  keyPressPhone(e) {
    this.phonePattren = /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/.test(
      e.target.value
    );

    if (e.target.value.length >= 7) {
      this.phonePattren = true;
    }
  }
}
