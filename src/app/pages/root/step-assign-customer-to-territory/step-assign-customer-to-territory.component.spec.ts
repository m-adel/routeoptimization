import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepAssignCustomerToTerritoryComponent } from './step-assign-customer-to-territory.component';

describe('StepAssignCustomerToTerritoryComponent', () => {
  let component: StepAssignCustomerToTerritoryComponent;
  let fixture: ComponentFixture<StepAssignCustomerToTerritoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepAssignCustomerToTerritoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepAssignCustomerToTerritoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
