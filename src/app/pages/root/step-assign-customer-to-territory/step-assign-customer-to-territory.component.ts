import { Component, OnInit, ViewChild } from "@angular/core";
import { TerritoryCustomerAssign } from "../interface/territory-customer-assign";
import { Observable, Subject, merge } from "rxjs";
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, filter } from "rxjs/operators";
import { RootService } from "../service/root.service";
import { Router } from '@angular/router';
import { SetpObserveService } from '../service/setp-observe.service';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { CustomerService } from './../../customer/service/customer.service';

@Component({
  selector: "app-step-assign-customer-to-territory",
  templateUrl: "./step-assign-customer-to-territory.component.html",
  styleUrls: ["./step-assign-customer-to-territory.component.sass"]
})
export class StepAssignCustomerToTerritoryComponent implements OnInit {
  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  searchCustomer:any;
  offset = 0;
  pageSize: any = 10;

  arrayOfCountOfCustomer = [];
  searching = false;
  searchFailed = false;
  arr = [];
  customerTerritory: TerritoryCustomerAssign = {} as TerritoryCustomerAssign;
  singleCustomerName: string;
  arrayOfCustomers =  [];
  active = false;
  success = false;
  error = false;
  Responsemessage = "";
  rootId: any;
  editFlag = false;
  loaded: boolean;
  arrayOfCustomerSelected: any;
  resetAllValues: boolean;
  clearValue: boolean;
  arrayOfCustomersName: any;
  constructor(public rootProvider: RootService, public router: Router, private stepProvider: SetpObserveService,
    private customerProvider: CustomerService ) {}
  formatter = (result: string) => result.toUpperCase();


  // search = (text$: Observable<string>) =>
  //   text$.pipe( debounceTime(300), distinctUntilChanged(), tap(() => (this.searching = true)),
  //   switchMap(term => this.rootProvider.search(term).pipe( tap(),
  //         catchError(() => {this.searchFailed = true; return [];
  //         }))), tap(() => (this.searching = false)))

  search = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.searchCustomer.slice(0,10) :
       this.searchCustomer.filter(v => v.toLowerCase().indexOf(term.toLowerCase())> -1))))
    }




  ngOnInit() {
    this.stepProvider.ObservableItem("stepThree");

    this.active = false;
    this.rootId = sessionStorage.getItem("editRootId");
    this.getCurrentCustomers();
    this.getAllCustomers()

  }

  getAllCustomers = () => {
    this.customerProvider.getAllCustomerForSingleCompany(this.offset, 100).subscribe((Res: any) => {
      this.searchCustomer =Res.map(v=>v.name1);


      this.loaded = false;
      this.searching = false;

    });
  }

  getCurrentCustomers() {
    this.rootProvider.getCurrentCustomerTerritory(this.rootId).subscribe((Res: any) => {
      Res.length > 0 ? this.editFlag = true : this.editFlag = false;
      if (this.editFlag) {
        this.active = true;
        this.customerTerritory = Res;
        this.arr = Res;
      }
      const ArrayOfCustomerCode  = Res.map((v ) => v.active == true ? ({customerCode: v.id.customerCode}) : "");
      this.getArrayOfCustomerNames(ArrayOfCustomerCode);
    });
  }
  getArrayOfCustomerNames(arr) {
    this.rootProvider.searchCustomerArray(arr).subscribe((Res) => {
      Res.forEach((v) => v.forEach(el => {
        this.arr.forEach(ele => {
          if (ele.id.customerCode == el.customerCode) {
            this.arrayOfCustomers.push(el);


          }
        });
      }));
    });
  }
  onChange(e) {
    const compayCode = sessionStorage.getItem("companyCode");
    const customerName = e.item;
    this.rootProvider.searchOnCustomers(customerName).subscribe((Res: any) => {
      this.arr.push({id: {
        customerCode: Res[0].customerCode,
        companyCode: compayCode,
        territoryCode: sessionStorage.getItem("addedTerritoryCode") || this.rootId
      },
      active: this.active
    });

      let index = this.searchCustomer.findIndex( x => x === customerName)
      this.arrayOfCustomers.push(Res[0]);
      this.searchCustomer.splice(index,1)

      console.log(this.arrayOfCustomers);

      this.singleCustomerName = "";
      this.customerTerritory = this.arr;
      console.log(this.arr);

    });





  }
  onChangeSwitchToggle(event) {
    this.arr.forEach(v => v.active = event);
    this.customerTerritory = this.arr;
  }
  handleSpliceArrayOfCustomer( code, index) {
    if (this.editFlag == true) {
      this.rootProvider.UnAssignCustomerTerritory(this.rootId, code).subscribe((Res: any) => {
        this.Responsemessage = "Successfully !";
        this.success = true;
        this.arrayOfCustomers.splice(index, 1);

        this.arr.forEach((v, i) => v.id.customerCode == Res.id.customerCode ? this.arr[i].active = false : "" );
        this.arr.splice(index, 1);
        this.customerTerritory = this.arr;
        setTimeout(() => this.success = false , 3000);
      }, err => {
        this.Responsemessage = err.error.message;
        this.error = true;
        setTimeout(() => this.error = false , 3000);
      });
    } else {
      this.arrayOfCustomers.splice(index, 1);
      this.arr.splice(index, 1);
      this.customerTerritory = this.arr;
    }
  }
  handleFinish() {
    this.rootProvider.AssignCustomerTerritory({territoryCustomers: this.customerTerritory}).subscribe((Res: any) => {
     this.success = true;
     this.error = false;
     this.Responsemessage = " Successfully !";
     setTimeout(() => {
       sessionStorage.removeItem("addedTerritoryCode");
       sessionStorage.removeItem("editRootId");
       this.router.navigateByUrl("/pages/root/all-roots");
       this.success = false;
     }, 2000);
    }, err => {

      this.error = true;
      this.Responsemessage = err.error.message;
      setTimeout(()=>{
          this.error = false
      },3000)
    });
    this.stepProvider.ObservableItem("stepTwo");

  }
  onPressBack() {
    if (this.router.url === "/pages/root/add-root/step-two-territory") {
      this.router.navigateByUrl('/pages/root/add-root/step-two-territory');
    } else {
      this.router.navigateByUrl(`/pages/root/edit-root/${this.rootId}/step-two-territory`);
    }
    this.stepProvider.ObservableItem("stepTwo");

  }
}
