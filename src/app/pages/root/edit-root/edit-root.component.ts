import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SetpObserveService } from '../service/setp-observe.service';

@Component({
  selector: 'app-edit-root',
  templateUrl: './edit-root.component.html',
  styleUrls: ['./edit-root.component.sass']
})
export class EditRootComponent implements OnInit {
  btnStepOne = "btn btn-info";
  btnStepTwo = "btn btn-light";
  btnStepThree = "btn btn-light";
  btnStepFour = "btn btn-light";
  rootId: any;
  constructor(
    public router: Router,
    public activeRoute: ActivatedRoute,
    private stepProvider: SetpObserveService
  ) {
    sessionStorage.removeItem("currentTaskId");
    sessionStorage.removeItem("currentTask");
    this.rootId = this.activeRoute.snapshot.params.id;
    sessionStorage.setItem("editRootId", this.rootId);
  }

  ngOnInit() {
    this.btnStepOne = "btn btn-info";
    this.btnStepTwo = "btn btn-light";
    this.btnStepThree = "btn btn-light";
    this.btnStepFour = "btn btn-light";
    this.stepProvider.getstep$().subscribe(Res => {
      switch (Res) {
        case "stepOne":
          this.btnStepOne = "btn btn-info";
          this.btnStepTwo = "btn btn-light";
          this.btnStepThree = "btn btn-light";
          this.btnStepFour = "btn btn-light";
          break;
        case "stepTwo":
          this.btnStepOne = "btn btn-light";
          this.btnStepTwo = "btn btn-info";
          this.btnStepThree = "btn btn-light";
          this.btnStepFour = "btn btn-light";
          break;
        case "stepThree":
          this.btnStepOne = "btn btn-light";
          this.btnStepTwo = "btn btn-light";
          this.btnStepThree = "btn btn-info";
          this.btnStepFour = "btn btn-light";
          break;
        case "stepFour":
          this.btnStepOne = "btn btn-light";
          this.btnStepTwo = "btn btn-light";
          this.btnStepThree = "btn btn-light";
          this.btnStepFour = "btn btn-info";
          break;
        default: break;
      }
    });
  }
  onPressStepOne() {
    this.router.navigateByUrl(`/pages/root/edit-root/${this.rootId}/step-one-territory`);
    this.btnStepOne = "btn btn-info";
    this.btnStepTwo = "btn btn-light";
    this.btnStepThree = "btn btn-light";
    this.btnStepFour = "btn btn-light";
  }
  onPressStepTwo() {
    this.router.navigateByUrl(`/pages/root/edit-root/${this.rootId}/step-two-territory`);
    this.btnStepOne = "btn btn-light";
    this.btnStepTwo = "btn btn-info";
    this.btnStepThree = "btn btn-light";
    this.btnStepFour = "btn btn-light";
  }
  onPressStepThree() {
    this.router.navigateByUrl(`/pages/root/edit-root/${this.rootId}/step-three-territory`);
    this.btnStepOne = "btn btn-light";
    this.btnStepTwo = "btn btn-light";
    this.btnStepThree = "btn btn-info";
    this.btnStepFour = "btn btn-light";
  }

}

