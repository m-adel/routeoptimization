import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AllRootsComponent } from "./all-roots/all-roots.component";
import { AddRootComponent } from "./add-root/add-root.component";
import { EditRootComponent } from "./edit-root/edit-root.component";
import { RootComponent } from "./root.component";
import { RootRoutingModule } from "./root.routing";
import { SharedModule } from "src/app/shared/shared.module";
import { UiSwitchModule } from 'ngx-toggle-switch';
import { StepTerritoryComponent } from './step-territory/step-territory.component';
import { StepUserTerritoryComponent } from './step-user-territory/step-user-territory.component';
import { StepAssignCustomerToTerritoryComponent } from './step-assign-customer-to-territory/step-assign-customer-to-territory.component';
import { RootMapComponent } from './component/root-map/root-map.component';
import { AgmCoreModule } from '@agm/core';
import { SingleCustomerTerritoryComponent } from './component/single-customer-territory/single-customer-territory.component';
import { AddUserModalComponent } from './component/add-user-modal/add-user-modal.component';
import { EditRouteComponent } from './edit-route/edit-route.component';

@NgModule({
  declarations: [
    AllRootsComponent,
    AddRootComponent,
    EditRootComponent,
    RootComponent,
    StepTerritoryComponent,
    StepUserTerritoryComponent,
    StepAssignCustomerToTerritoryComponent,
    RootMapComponent,
    SingleCustomerTerritoryComponent,
    AddUserModalComponent,
    EditRouteComponent
  ],
  imports: [
    CommonModule,
    RootRoutingModule,
    FormsModule,
    SharedModule,
    UiSwitchModule,
    AgmCoreModule,
    ReactiveFormsModule
  ]
})
export class RootModule {}
