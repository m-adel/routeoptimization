import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';
import { AddRootService } from '../service/add-root.service';
import { UserService } from "../../user/service/user.service";
import { RootService } from "../service/root.service";
import { UserTerritory } from '../interface/user-territory';
import {Location} from '@angular/common';

@Component({
  selector: 'app-edit-route',
  templateUrl: './edit-route.component.html',
  styleUrls: ['./edit-route.component.scss']
})
export class EditRouteComponent implements OnInit {
  routeId:any
  editFlag = false;
  orignalData = {user: {userId: ""}};
  regions:any;
  regionsNames:any;



  public userTerritory: UserTerritory = { territory: {}, user: {} } as UserTerritory;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private addRootProvider: AddRootService,
    public userProvider: UserService,
    public rootProvider: RootService,
    private location: Location,

  ) {
    this.routeId = route.snapshot.paramMap.get('id')
    console.log(this.routeId);

    this.getCuurentUserTerritory()

  }

  ngOnInit() {
    this.getCuurentUserTerritory()
    this.getAllRegion()
  }
  getCuurentUserTerritory() {
    this.rootProvider.getUserCurrentTerritory(this.routeId).subscribe((Res: any) => {
      this.editFlag = Res.length > 0 ? true : false;
      if (this.editFlag) {
        this.userTerritory = Res[0];
        console.log(this.userTerritory);

        this.orignalData = JSON.parse(JSON.stringify(this.userTerritory));
      }
    });
  }
  getAllRegion = () => {
    this.addRootProvider.getAllRegions().subscribe((Res: any) => {
      this.regions = Res;
      console.log(this.regions);

      this.regionsNames = Res.map(v => v.regionName);
    });
  }
}
