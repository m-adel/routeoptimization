import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RootService } from '../service/root.service';

@Component({
  selector: 'app-all-roots',
  templateUrl: './all-roots.component.html',
  styleUrls: ['./all-roots.component.sass']
})
export class AllRootsComponent implements OnInit {
  data = [];
  originalData = [];
  page: any = 1;
  pageSize: any = 10;
  totalItems: any = 0;
  offset = 0;
  Responsemessage = "";
  error = false;
  seaching = false;
  loaded = true;
  RootId: any;
  indexOfRootId: any;
  constructor(public router: Router, public rootProvider: RootService) { }

  ngOnInit() {
    this.rootProvider.getCountOfTrritory().subscribe((Res: any) => {
      this.totalItems = Res;
      this.getAllRoots();
    });
  }
  getAllRoots() {
    this.rootProvider.getAllTrritiories(this.offset, this.pageSize).subscribe((Res: any) => {
      console.log(Res);
      this.data = Res;
      this.originalData = Res;
      this.loaded = false;
      this.seaching = false;
    }, err => {
      this.Responsemessage = err.error.message;
      this.error = true;
      setTimeout(() => this.error = false, 2000);
    });
  }
  PageChange(e) {
    this.loaded = true;
    this.page = e - 1;
    this.offset = this.page * this.pageSize;
    this.ngOnInit();
  }
  onPressEditIcon(id) {
    this.router.navigate(["/pages/root/edit-root", id]);
  }

  onChange(e) {
    this.offset = 0;
    const UserName = e.target.value;
    if (UserName == "") {
      this.seaching = false;
      this.ngOnInit();
    } else {
      this.rootProvider.searchByName(UserName, this.offset, this.totalItems).subscribe((Res: any) => {
          this.data = Res;
          this.seaching = true;
          if (UserName == "") {
            this.seaching = false;
            this.ngOnInit();
          }
      });
    }
  }
  onPressDeleteIcon(user, index) {
    this.RootId = user;
    console.log(this.RootId);

    this.indexOfRootId = index;
    console.log(this.indexOfRootId);

  }
  handelDeleteUser() {
    this.rootProvider.deletedTerritory(this.RootId).subscribe((Res: any) => {
      this.data.splice(this.indexOfRootId, 1);
      console.log(Res);

    }, err => {
      console.log(err);
      if (err.error.text === "Entity deleted successfully") {
        this.data.splice(this.indexOfRootId, 1);
        this.Responsemessage = "Deleted Successfully";
        this.error = true;
        setTimeout(() => this.error = false, 2000);
      } else if(err.error.status == 404) {
        this.Responsemessage = "this Felid is maping for Other Felid";
        this.error = true;
        setTimeout(() => this.error = false, 2000);
      } else {
        this.Responsemessage = err.error.message;
        this.error = true;
        setTimeout(() => this.error = false, 2000);
      }
    });
  }

}
