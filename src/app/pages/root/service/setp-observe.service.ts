import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SetpObserveService {
  task: any;
  public step: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  constructor() {}

  getstep$() {
    return this.step.asObservable();

  }

  ObservableItem(step) {
    this.step.next(step);
  }
  manageStateForTask(object) {
    this.task = object;
    sessionStorage.setItem("currentTaskId", this.task.taskId);
    sessionStorage.setItem("currentTaskVersion", this.task.version);
  }

}

