import { Injectable } from '@angular/core';
import { WebServicesService } from 'src/app/providers/web-services.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddRootService {

  constructor(public http: HttpClient, public config: WebServicesService) { }
  getAllCountries() {
    const url = this.config.serviceUrl + this.config.getAllCountries;
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getAllRegions() {
    const url = this.config.serviceUrl + this.config.getAllRegions;
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getAllGovernoments() {
    const url = this.config.serviceUrl + this.config.getAllGovernoment;
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  addTerritory(data) {
    const url = this.config.serviceUrl + this.config.addTerritory.
    replace("{companyCode}", sessionStorage.getItem("companyCode"));
    const header = this.config.headers;
    return this.http.post(url, data, {headers: header});
  }
  addTerritorySalesData(data) {
    const url = this.config.serviceUrl + this.config.addTerritoryDataSales;
    const header = this.config.headers;
    return this.http.put(url, data, {headers: header});
  }
  getCountOfAllTerritory() {
    const url = this.config.serviceUrl + this.config.countOfRootForSingleCompany.
    replace("{companyCode}", "");
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getAllTrritiories(offset, limit) {
    const url = this.config.serviceUrl + this.config.getAllRootsForCompony.replace("{0}", "").
    replace("{1}", offset).replace("{2}", limit);
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getCurrentTerritory(id) {
    const url = this.config.serviceUrl + this.config.getCuurentTerritory.
    replace("{0}", id).replace("{1}", sessionStorage.getItem("companyCode"));
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  editedTerritory(data) {
    const url = this.config.serviceUrl + this.config.editTerritory;
    const header = this.config.headers;
    return this.http.put(url, data , {headers: header});
  }
}
