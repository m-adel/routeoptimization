import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { WebServicesService } from 'src/app/providers/web-services.service';
import { map } from 'rxjs/internal/operators/map';
import { of, Observable } from 'rxjs';
import 'rxjs/add/observable/forkJoin'

@Injectable()
export class RootService {

  constructor(public http: HttpClient, public config: WebServicesService,) { }
  getCountOfTrritory() {
    const url = this.config.serviceUrl + this.config.countOfRootForSingleCompany.
    replace("{companyCode}", sessionStorage.getItem("companyCode"));
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getAllTrritiories(offset, limit) {
    const url = this.config.serviceUrl + this.config.getAllRootsForCompony.replace("{0}", sessionStorage.getItem("companyCode")).
    replace("{1}", offset).replace("{2}", limit);
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  searchByName(name, offset, limit) {
    const url = this.config.serviceUrl + this.config.searchByRootName.replace("{1}", name).
    replace("{2}", sessionStorage.getItem("companyCode")).replace("{3}", offset).replace("{4}", limit);
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  deletedTerritory(code) {
    const url = this.config.serviceUrl + this.config.deleteRoot.replace("{territoryCode}", code);
    const header = this.config.headers;
    return this.http.delete(url, {headers: header});
  }
  addUserTerritory(data) {
    const url = this.config.serviceUrl + this.config.addUserTerritories;
    const header = this.config.headers;
    return this.http.post(url, data, {headers: header});
  }

  editUserTerritory(data) {
    const url = this.config.serviceUrl + this.config.addUserTerritories;
    const header = this.config.headers;
    return this.http.put(url, data, {headers: header});
  }

  searchOnCustomers(name) {
    const url = this.config.serviceUrl + this.config.searchOfCustomerForTerritory.replace("{0}", name).
    replace("{1}", sessionStorage.getItem("companyCode"));
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  search(term) {
    let arr = [];
    const url = this.config.serviceUrl + this.config.searchOfCustomerForTerritory.
    replace("{0}", term).replace("{1}", sessionStorage.getItem("companyCode"));
    const header = this.config.headers;
    if (term === '') {
      return of([]);
    }
    return this.http
      .get(url, {headers: header}).pipe(
        map((response: any) => {
         arr = response.map(element => {
           return element.name1;
         });
         return arr;
        })
      );
}
AssignCustomerTerritory(data) {
  const url = this.config.serviceUrl + this.config.AssignCustomerTerritory;
  const header = this.config.headers;
  header.append('Content-Type', 'application/json');
  return this.http.put(url, data, {headers: header});
}
getCurrentCustomerTerritory(id) {
  const url = this.config.serviceUrl + this.config.currentCustomerTerritory.
  replace("{0}", sessionStorage.getItem("companyCode")).replace("{1}", id);
  const header = this.config.headers;
  return this.http.get(url, {headers: header});
}
getUserCurrentTerritory(id) {
  const url = this.config.serviceUrl + this.config.currentUserTerritory.replace("{0}", id);
  const header = this.config.headers;
  return this.http.get(url, {headers: header});
}
searchCustomerArray(arrayOfCustomerCode) {
  const observableBatch = [];
  arrayOfCustomerCode.forEach(( v, key ) => {
    const url = this.config.serviceUrl + this.config.getCustomerNamesAssignedToRoot.
    replace("{0}", v.customerCode).replace("{1}", sessionStorage.getItem("companyCode"));
    const header = this.config.headers;
    observableBatch.push(this.http.get(url, {headers: header}));
  });
  return Observable.forkJoin(observableBatch);

}
UnAssignCustomerTerritory(rootCode, cusCode) {
  const url = this.config.serviceUrl + this.config.unassignCustomerTerritory.
  replace("{0}", sessionStorage.getItem("companyCode")).replace("{1}", rootCode).replace("{2}", cusCode);
  const header = this.config.headers;
  return this.http.put(url, null, {headers: header});
}

}
