import { TestBed } from '@angular/core/testing';

import { SetpObserveService } from './setp-observe.service';

describe('SetpObserveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SetpObserveService = TestBed.get(SetpObserveService);
    expect(service).toBeTruthy();
  });
});
