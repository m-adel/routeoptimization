export interface TerritorySalesData {
  id: IdTerritory;
  errorRadius: number;
  maximumVisitOutOfCloseDay: number;
  maximumVisitOutOfJourneyPlan: number;
  maximumVisitOutOfOrder: number;
  maximumVisitWithoutProof: number;
  proofOfVisit: string;
  recordCustomerAsset: boolean;
  recordCustomerGpsLocation: boolean;
}
interface IdTerritory {
  territoryCode: string;
  companyCode: string;
}
