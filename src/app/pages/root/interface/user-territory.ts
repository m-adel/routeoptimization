import { Territory } from './territory';

export interface UserTerritory {
  id: any;
  active: boolean;
  carNumber: string;
  territory: Territory;
  user: User;
}
interface User {
  userId: any;
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  phoneNumber: string;
  userName: string;
  profile: Profile;
}
interface Profile {
  profileId: any;
  profileCode: string;
  profileDescription: string;
}

