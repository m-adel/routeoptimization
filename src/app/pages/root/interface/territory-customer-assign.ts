export interface TerritoryCustomerAssign {
  [index: number]: {
    id: {
      territoryCode: number;
      companyCode: string;
      customerCode: number;
    };
    active: true;
  };
}
