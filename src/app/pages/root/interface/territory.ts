export interface Territory {
  territoryCode: string;
  active: boolean;
  name1: string;
  latitude: any;
  longitude: any;
  country: Country;
  governorate: Governoment;
  region: Region;
}
export interface Country {
  countryCode: string;
  countryName: string;
}
export interface Governoment {
  governorateCode: string;
  governorateName: string;
}
export interface Region {
  regionCode: string;
  regionName: string;
}
