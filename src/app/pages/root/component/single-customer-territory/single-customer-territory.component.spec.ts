import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleCustomerTerritoryComponent } from './single-customer-territory.component';

describe('SingleCustomerTerritoryComponent', () => {
  let component: SingleCustomerTerritoryComponent;
  let fixture: ComponentFixture<SingleCustomerTerritoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleCustomerTerritoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleCustomerTerritoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
