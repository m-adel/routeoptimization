/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild, ElementRef, NgZone, Output, EventEmitter, Input, OnChanges } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MapsAPILoader } from "@agm/core";
declare let google: any;
@Component({
  selector: "app-root-map",
  templateUrl: "./root-map.component.html",
  styleUrls: ["./root-map.component.sass"]
})
export class RootMapComponent implements OnInit , OnChanges {
  latitude: any;
  longitude: any;
  public searchControl: FormControl;
  public zoom: number;
  @Input() currentLat: any;
  @Input() curreentLng: any;
  @ViewChild("search") public searchElementRef: ElementRef;
  constructor(private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {}
  @Output() sendLatLng = new EventEmitter();
  ngOnInit() {
    this.searchControl = new FormControl();
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      this.latitude = this.currentLat ||  	30.0626297;
      this.longitude = this.curreentLng ||  31.24967;
      const latLng = {lat: this.latitude, lng: this.longitude};
      this.sendLatLng.emit(latLng);
      this.zoom = 16;
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          const newlatLng = {lat: this.latitude, lng: this.longitude};
          this.sendLatLng.emit(newlatLng);
          this.zoom = 10;
        });
      });
    });
  }
  ngOnChanges() {
    setTimeout(() => {
      this.latitude = this.currentLat ||  30.0626297;
      this.longitude = this.curreentLng || 31.24967;


      this.zoom = 10;
    }, 200);

  }
  markerDragEnd(event) {
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
    const latLng = {lat: this.latitude, lng: this.longitude};
    this.sendLatLng.emit(latLng);
  }
}
