import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RootMapComponent } from './root-map.component';

describe('RootMapComponent', () => {
  let component: RootMapComponent;
  let fixture: ComponentFixture<RootMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RootMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RootMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
