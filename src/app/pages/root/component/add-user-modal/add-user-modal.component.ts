import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/pages/user/service/user.service';
import { HandleErrorService } from 'src/app/providers/handle-error.service';

@Component({
  selector: 'app-add-user-modal',
  templateUrl: './add-user-modal.component.html',
  styleUrls: ['./add-user-modal.component.sass']
})
export class AddUserModalComponent implements OnInit {
  @Output() newUser = new EventEmitter();
  user = {
    email: "",
    firstName: "",
    lastName: "",
    password: "",
    phoneNumber: "",
    userName: "",
    confirmPassword: "",
    profile: {
      profile: 0,
      profileCode: "",
      profileDescription: ""
    }
  };
  mustMatch = false;
  profiles = [];
  loaded = true;
  success = false;
  Responsemessage;
  error = false;
  phonePattren = true;
  constructor(
    public router: Router,
    private userProvider: UserService,
    private errorHandle: HandleErrorService
  ) { }

  ngOnInit() {
    this.getAllProfiles();
  }
  getAllProfiles() {
    this.userProvider.getAllProfil().subscribe((Res: any) => {
      this.profiles = Res;
      this.loaded = false;
      this.profiles.forEach(el => el.profileCode == "USR" ? this.user.profile = el : "");
    });
  }
  onChangeCurrentPassword(e) {
    this.mustMatch = (this.user.confirmPassword === this.user.password ? false : true);
  }
  onSelectProfile(event) {
    this.profiles.forEach(el => this.user.profile.profileCode === el.profileCode ? this.user.profile = el : "");
  }
  handleAddUser() {
    this.userProvider.createNewUser(this.user).subscribe((Res: any) => {
      this.Responsemessage = "Creating Successfully !";
      this.newUser.emit(Res);
      this.success = true;
      setTimeout(() => {
        this.success = false;
      }, 2000);
    }, err => {
      this.error = true;
      this.Responsemessage = this.errorHandle.errorHandler(err.status, "pages");
    });
  }
  handleBackToAllUser() {
    this.router.navigateByUrl("/pages/user/all-users");
  }
  keyPressPhone(e) {
    this.phonePattren = /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/.test(e.target.value);
    if (e.target.value.length >= 11 && e.target.value.length <= 13) {
      this.phonePattren = true;
    }
  }

}
