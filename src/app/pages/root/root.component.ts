import { Component } from '@angular/core';

@Component({
  selector: 'app-Root',
  template: `
  <!-- <app-child-navs [navs]="navs"></app-child-navs> -->

  <router-outlet></router-outlet>
  `
})
export class RootComponent {
  navs = [
    { name: "All Routes", link: "/pages/root/all-roots" },
    { name: "Add Route", link: "/pages/root/add-root" },
  ];
}
