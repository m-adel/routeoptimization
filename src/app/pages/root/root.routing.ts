import { AllRootsComponent } from "./all-roots/all-roots.component";
import { EditRootComponent } from "./edit-root/edit-root.component";
import { AddRootComponent } from "./add-root/add-root.component";

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RootComponent } from "./root.component";
import { StepTerritoryComponent } from "./step-territory/step-territory.component";
import { StepUserTerritoryComponent } from "./step-user-territory/step-user-territory.component";
import { StepAssignCustomerToTerritoryComponent } from "./step-assign-customer-to-territory/step-assign-customer-to-territory.component";
import { EditRouteComponent } from './edit-route/edit-route.component';

const routes: Routes = [
  {
    path: "",
    component: RootComponent,
    children: [
      {
        path: "add-root",
        component: AddRootComponent,
        children: [
          {
            path: "step-one-territory",
            component: StepTerritoryComponent
          },
          {
            path: "step-two-territory",
            component: StepUserTerritoryComponent
          },
          {
            path: "step-three-territory",
            component: StepAssignCustomerToTerritoryComponent
          },
          {
            path: "",
            redirectTo: "step-one-territory",
            pathMatch: "full"
          }
        ]
      },
      {
        path: "edit-root/:id",
        component: EditRootComponent,
        children: [
          {
            path: "step-one-territory",
            component: StepTerritoryComponent
          },
          {
            path: "step-two-territory",
            component: StepUserTerritoryComponent
          },
          {
            path: "step-three-territory",
            component: StepAssignCustomerToTerritoryComponent
          },
          {
            path: "",
            redirectTo: "step-one-territory",
            pathMatch: "full"
          }
        ]
      },
      {
        path: "all-roots",
        component: AllRootsComponent
      },
      {
        path: "",
        redirectTo: "/pages/root/all-roots",
        pathMatch: "full"
      },
      {
        path: "edit-route/:id",
        component: EditRouteComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RootRoutingModule {}
