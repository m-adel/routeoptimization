import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SetpObserveService } from '../service/setp-observe.service';

@Component({
  selector: 'app-add-root',
  templateUrl: './add-root.component.html',
  styleUrls: ['./add-root.component.sass']
})
export class AddRootComponent implements OnInit {
  btnStepOne = "btn btn-info";
  btnStepTwo = "btn btn-light";
  btnStepThree = "btn btn-light";
  btnStepFour = "btn btn-light";
  constructor(
    public router: Router,
    private stepProvider: SetpObserveService
  ) {
    sessionStorage.removeItem("currentTaskId");
    sessionStorage.removeItem("currentTask");
    sessionStorage.removeItem("editRootId");
  }

  ngOnInit() {
    this.btnStepOne = "btn btn-info";
    this.btnStepTwo = "btn btn-light";
    this.btnStepThree = "btn btn-light";
    this.btnStepFour = "btn btn-light";
    this.stepProvider.getstep$().subscribe(Res => {
      switch (Res) {
        case "stepOne":
          this.btnStepOne = "btn btn-info";
          this.btnStepTwo = "btn btn-light";
          this.btnStepThree = "btn btn-light";
          this.btnStepFour = "btn btn-light";
          break;
        case "stepTwo":
          this.btnStepOne = "btn btn-light";
          this.btnStepTwo = "btn btn-info";
          this.btnStepThree = "btn btn-light";
          this.btnStepFour = "btn btn-light";
          break;
        case "stepThree":
          this.btnStepOne = "btn btn-light";
          this.btnStepTwo = "btn btn-light";
          this.btnStepThree = "btn btn-info";
          this.btnStepFour = "btn btn-light";
          break;
        case "stepFour":
          this.btnStepOne = "btn btn-light";
          this.btnStepTwo = "btn btn-light";
          this.btnStepThree = "btn btn-light";
          this.btnStepFour = "btn btn-info";
          break;
        default: break;
      }
    });
  }
  onPressStepOne() {
    this.router.navigateByUrl("/pages/root/add-root/step-one-territory");
    this.btnStepOne = "btn btn-info";
    this.btnStepTwo = "btn btn-light";
    this.btnStepThree = "btn btn-light";
    this.btnStepFour = "btn btn-light";
  }
  onPressStepTwo() {
    this.router.navigateByUrl("/pages/root/add-root/step-two-territory");
    this.btnStepOne = "btn btn-light";
    this.btnStepTwo = "btn btn-info";
    this.btnStepThree = "btn btn-light";
    this.btnStepFour = "btn btn-light";
  }
  onPressStepThree() {
    this.router.navigateByUrl("/pages/root/add-root/step-three-territory");
    this.btnStepOne = "btn btn-light";
    this.btnStepTwo = "btn btn-light";
    this.btnStepThree = "btn btn-info";
    this.btnStepFour = "btn btn-light";
  }

}

