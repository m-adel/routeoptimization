import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddRootService } from '../service/add-root.service';
import { Territory } from '../interface/territory';
import { TerritorySalesData } from '../interface/territory-sales-data';
import { RootService } from '../service/root.service';
import { SetpObserveService } from '../service/setp-observe.service';

@Component({
  selector: 'app-step-territory',
  templateUrl: './step-territory.component.html',
  styleUrls: ['./step-territory.component.sass']
})
export class StepTerritoryComponent implements OnInit {
  countries = [];
  countriesName = [];
  regions = [];
  regionsNames = [];
  govers = [];
  goverNames = [];
  loaded = true;
  marked = false
  territory: Territory = {} as Territory;
  success = false;
  Responsemessage = "";
  theCheckbox
  error = false;
  rootId: any;
  public territorySalesData: TerritorySalesData = { id: {} } as TerritorySalesData;

  constructor(
    public router: Router,
    private addRootProvider: AddRootService,
    private stepProvider: SetpObserveService

  ) { }

  ngOnInit() {
    this.territorySalesData.maximumVisitWithoutProof = 0;
    this.stepProvider.ObservableItem("stepOne");
    this.rootId = sessionStorage.getItem("editRootId");
    this.territory.active = false;
    this.territorySalesData.recordCustomerGpsLocation = false;
    this.territorySalesData.recordCustomerAsset = false;
    const companyCode = sessionStorage.getItem("companyCode");
    this.territorySalesData.id.companyCode = companyCode;
    this.getAllCountries();
    this.getAllRegion();
    this.getAllGovernoments();
    this.getCountOfTerritory();
    if (this.rootId) {
      this.getCurrentTerritory();
    }
  }
  toggleVisibility(e){
    this.marked= e.target.checked;
  }


  getCurrentTerritory = () => {
    this.addRootProvider.getCurrentTerritory(this.rootId).subscribe((Res: any) => {
      this.territory = Res.territory;
      this.territorySalesData = Res;
    });
  }
  getAllCountries = () => {
    this.addRootProvider.getAllCountries().subscribe((Res: any) => {
      this.loaded = false;
      this.countries = Res.countries;
      this.countriesName = Res.countries.map(v => v.countryName);
      this.countries.map(v => v.countryName === "Egypt" ? this.territory.country = v : "");
    });
  }
  getAllRegion = () => {
    this.addRootProvider.getAllRegions().subscribe((Res: any) => {
      this.regions = Res;
      this.regionsNames = Res.map(v => v.regionName);
    });
  }
  getAllGovernoments = () => {
    this.addRootProvider.getAllGovernoments().subscribe((Res: any) => {
      this.govers = Res;
      this.goverNames = Res.map(v => v.governorateName);
    });
  }
  getCountOfTerritory = () => {
    if (!this.rootId) {
      this.addRootProvider.getCountOfAllTerritory().subscribe((Res: any) => {
        if (Res > 0) {
          this.addRootProvider.getAllTrritiories(Res - 1, 1).subscribe((Resp) => {
            const lastProductMaterialCode = (+Resp[0].territoryCode + + 1).toString();
            const length = 10 - lastProductMaterialCode.toString().split("").length;
            const newArr = [];
            for (let index = 0; index < length; index++) {
              newArr.push("0");
            }
            this.territory.territoryCode = newArr.join("") + lastProductMaterialCode;
            this.territorySalesData.id.territoryCode = this.territory.territoryCode;
          });
        } else if (Res == 0) {
          this.territory.territoryCode = "0000100100";
          this.territorySalesData.id.territoryCode = "0000100100";
        }
      });
    }
  }
  onChange(event) {
    const value = event.selectedData.toUpperCase();
    switch (event.DataFrom) {
      case "country": this.countries.forEach(v => v.countryName.toUpperCase() === value ? this.territory.country = v : ""); break;
      case "governoment": this.govers.forEach(v => v.governorateName.toUpperCase() === value ? this.territory.governorate = v : ""); break;
      case "region": this.regions.forEach(v => v.regionName.toUpperCase() === value ? this.territory.region = v : ""); break;
      default: break;
    }
  }
  onChangeLatLng(event) {
    this.territory.latitude = event.lat;
    this.territory.longitude = event.lng;
  }

  handleNext() {
    !this.rootId ? this.addTerritory() : this.editTerritory();
  }
  addTerritory() {
    console.log(this.territorySalesData);
    this.addRootProvider.addTerritory(this.territory).subscribe((Res: any) => {
      sessionStorage.setItem("editRootId", this.territory.territoryCode);
      this.putTerritoryDataSales();
    }, err => {
      this.error = true;
      this.Responsemessage = "you must fill all fields";
      // setTimeout(()=>{
      //     this.error = false
      // },3000)
    });
  }
  putTerritoryDataSales() {
    this.territorySalesData.id.territoryCode = this.rootId || this.territory.territoryCode;
    this.addRootProvider.addTerritorySalesData(this.territorySalesData).subscribe((Resp: any) => {
      this.success = true;
      this.error = false;
      this.Responsemessage = "Successfully !";
      setTimeout(() => {
        this.success = false;
        if (this.router.url === "/pages/root/add-root/step-one-territory") {
          this.router.navigateByUrl('/pages/root/add-root/step-two-territory');
        } else {
          this.router.navigateByUrl(`pages/root/edit-root/${this.rootId}/step-two-territory`);
        }
        this.stepProvider.ObservableItem("stepTwo");
      }, 2000);
    }, err => {
      this.error = true;
      this.Responsemessage = err.error.message;
      setTimeout(()=>{
          this.error = false
      },3000)
    });
  }
  editTerritory() {
    this.territory.territoryCode = this.rootId;
    this.addRootProvider.editedTerritory(this.territory).subscribe((Res: any) => {
      this.putTerritoryDataSales();
    }, err => {
      this.error = true;
      this.Responsemessage = err.error.message;
      setTimeout(()=>{
          this.error = false
      },3000)
    });
  }

}
