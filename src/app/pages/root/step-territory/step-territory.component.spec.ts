import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepTerritoryComponent } from './step-territory.component';

describe('StepTerritoryComponent', () => {
  let component: StepTerritoryComponent;
  let fixture: ComponentFixture<StepTerritoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepTerritoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepTerritoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
