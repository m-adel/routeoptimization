import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepUserTerritoryComponent } from './step-user-territory.component';

describe('StepUserTerritoryComponent', () => {
  let component: StepUserTerritoryComponent;
  let fixture: ComponentFixture<StepUserTerritoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepUserTerritoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepUserTerritoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
