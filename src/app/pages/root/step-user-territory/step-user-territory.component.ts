import { Component, OnInit } from "@angular/core";
import { UserService } from "../../user/service/user.service";
import { RootService } from "../service/root.service";
import { UserTerritory } from '../interface/user-territory';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import { SetpObserveService } from '../service/setp-observe.service';

@Component({
  selector: "app-step-user-territory",
  templateUrl: "./step-user-territory.component.html",
  styleUrls: ["./step-user-territory.component.sass"]
})
export class StepUserTerritoryComponent implements OnInit {
  loaded = true;
  userNames = [];
  users = [];
  roots = [];
  success = false;
  Responsemessage = "";
  error = false;
  rootId: any;
  editFlag = false;
  orignalData = {user: {userId: ""}};
  public userTerritory: UserTerritory = { territory: {}, user: {} } as UserTerritory;

  constructor(
    public userProvider: UserService,
    public rootProvider: RootService,
    public router: Router,
    private location: Location,
    private stepProvider: SetpObserveService

  ) {}

  ngOnInit() {
    this.stepProvider.ObservableItem("stepTwo");
    this.rootId = sessionStorage.getItem("editRootId");
    this.getAllUsers();
    this.getAllRoots();
    this.getCuurentUserTerritory();
  }
  getCuurentUserTerritory() {
    this.rootProvider.getUserCurrentTerritory(this.rootId).subscribe((Res: any) => {
      this.editFlag = Res.length > 0 ? true : false;
      if (this.editFlag) {
        this.userTerritory = Res[0];
        this.orignalData = JSON.parse(JSON.stringify(this.userTerritory));
      }
    });
  }
  getAllUsers = () => {
    this.userProvider.getCountOfUser().subscribe(Res => {
      this.userProvider.getAllUsersForAddingTerritory(0, Res).subscribe((Resp: any) => {
        this.loaded = false;
        this.userNames = Resp.map(v => v.userName);
        this.users = Resp;
      });
    });
  }
  getAllRoots = () => {
    let RootCode = sessionStorage.getItem("addedTerritoryCode");
    RootCode = RootCode || this.rootId;
    this.rootProvider.getCountOfTrritory().subscribe(Res => {
      this.rootProvider.getAllTrritiories(0, Res).subscribe((Resp: any) => {
        this.roots = Resp;
        this.roots.forEach((v) => v.territoryCode == RootCode ? this.userTerritory.territory = v : "" );
      });
    });
  }
  onChange(event) {
    const value = event.selectedData.toUpperCase();
    this.users.forEach(v => v.userName.toUpperCase() == value ? this.userTerritory.user = v : "");
  }
  ReciveNewUser(event) {
  this.userTerritory.user = event;
  }
  handleNext() {
    this.editFlag ? this.editUserTerritory() : this.addUserTerritory();
  }
  editUserTerritory() {
    if (this.userTerritory.user.userId == this.orignalData.user.userId
      && this.userTerritory.carNumber == this.orignalData["carNumber"] &&
      this.userTerritory.active == this.orignalData["active"]
        ) {
      if (this.router.url === "/pages/root/add-root/step-two-territory") {
        this.router.navigateByUrl('/pages/root/add-root/step-three-territory');
      } else {
        this.router.navigateByUrl(`/pages/root/edit-root/${this.rootId}/step-three-territory`);
      }
      this.stepProvider.ObservableItem("stepThree");
    } else {
      this.rootProvider.editUserTerritory(this.userTerritory).subscribe((Res) => {
        this.stepProvider.ObservableItem("stepThree");
        this.success = true;
        this.error = false;
        this.Responsemessage = "Successfully !";
        setTimeout(() => {
          this.success = false;
          if (this.router.url === "/pages/root/add-root/step-two-territory") {
            this.router.navigateByUrl('/pages/root/add-root/step-three-territory');
          } else {
            this.router.navigateByUrl(`/pages/root/edit-root/${this.rootId}/step-three-territory`);
          }
        }, 2000);
      }, err => {
        this.error = true;
        this.Responsemessage = err.error.message;
        // setTimeout(()=>{
        //     this.error = false
        // },3000)
      });
    }
  }
  addUserTerritory() {
    this.rootProvider.addUserTerritory(this.userTerritory).subscribe((Res) => {
      this.success = true;
      this.error = false;
      this.Responsemessage = "Successfully !";
      setTimeout(() => {
        this.success = false;
        if (this.router.url === "/pages/root/add-root/step-two-territory") {
          this.router.navigateByUrl('/pages/root/add-root/step-three-territory');
        } else {
          this.router.navigateByUrl(`/pages/root/edit-root/${this.rootId}/step-three-territory`);
        }
        this.stepProvider.ObservableItem("stepThree");

      }, 2000);
    }, err => {
      this.error = true;
      this.Responsemessage = "you must fill all fields";
      // setTimeout(()=>{
      //     this.error = false
      // },3000)
    });
  }
  onPressBack() {
    if (this.router.url === "/pages/root/add-root/step-two-territory") {
      this.router.navigateByUrl('/pages/root/add-root/step-one-territory');
    } else {
      this.router.navigateByUrl(`/pages/root/edit-root/${this.rootId}/step-one-territory`);
    }
    this.stepProvider.ObservableItem("stepOne");

  }
}
