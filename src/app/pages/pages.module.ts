import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageComponent } from './page/page.component';
import { PageRoutingModule } from "src/app/pages/page.routing";
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [PageComponent, HomeComponent, ProfileComponent],
  imports: [
    CommonModule,
    PageRoutingModule,
    SharedModule,
    RouterModule,
    FormsModule
  ]
})
export class PagesModule { }
