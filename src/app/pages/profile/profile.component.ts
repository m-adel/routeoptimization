import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

  constructor(private userProvider: UserService, public router: Router) { }

  id: any;
  loaded = true;
  user = {
    firstName: "",
    lastName: "",
    password: "",
    phoneNumber: "",
    userName: "",
    confirmPassword: "",
    profile: {
      profile: 0,
      profileCode: "",
      profileDescription: ""
    }
  };
  success = false;
  Responsemessage = "";
  error = false;
  ngOnInit() {
    this.id = sessionStorage.getItem("userId");
    this.showUser();
  }
  showUser() {
    this.userProvider.showProfile(this.id).subscribe((Res: any) => {
      this.user = Res;
      this.user.confirmPassword = this.user.password;
      this.loaded = false;
    });
  }
  handleBackToGetAllUser() {
    this.router.navigateByUrl("/pages/dashboard/main-dashboard");
  }
}
