import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { CustomerContact } from '../../interface/customer-contact';

@Component({
  selector: 'app-customer-contact',
  templateUrl: './customer-contact.component.html',
  styleUrls: ['./customer-contact.component.sass']
})
export class CustomerContactComponent implements OnInit, OnChanges {
  customerContact: CustomerContact = {} as CustomerContact;
  @Input() customerCode: any;
  @Output() sendCustomerContact = new  EventEmitter();
  @Output() sendCustomerToSplice = new EventEmitter();
  @Input() dataSource: any;
  arrayOfContacts = [];
  indexOfCustomerContact: any;
  constructor() { }

  ngOnInit() {
    this.customerContact.active = false;
  }
  ngOnChanges() {
    if (this.dataSource) {
      this.customerContact = this.dataSource;
    }
  }
  onPressAdd() {
    this.customerContact.customerCode = this.customerCode;
    this.customerContact.companyCode = sessionStorage.getItem("companyCode");
    const newObj = JSON.parse(JSON.stringify(this.customerContact));
    this.arrayOfContacts.push(newObj);
    this.sendCustomerContact.emit(this.arrayOfContacts);
    this.resetAllValues();
  }
  resetAllValues() {
    this.customerContact.active = false;
    this.customerContact.mobile = "";
    this.customerContact.name = "";
    this.customerContact.telephone = "";
    this.customerContact.title = "";
  }
  onPressSpliceArrayOfContacts(i) {
    this.arrayOfContacts.splice(i, 1);
    this.sendCustomerContact.emit(this.arrayOfContacts);
  }
  onPressEditContact(obj, i) {
    this.customerContact = obj;
    this.indexOfCustomerContact = i;
  }
  onPressEdit() {
    const newObj = JSON.parse(JSON.stringify(this.customerContact));
    this.arrayOfContacts.splice(this.indexOfCustomerContact, 1, newObj);
    this.sendCustomerContact.emit(this.arrayOfContacts);
  }

}
