import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from "@angular/core";
import { CustomerAsset } from '../../interface/customer-asset';
import { CustomerService } from '../../service/customer.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-customer-assets",
  templateUrl: "./customer-assets.component.html",
  styleUrls: ["./customer-assets.component.sass"]
})
export class CustomerAssetsComponent implements OnInit {
  constructor(private customerProvider: CustomerService, public router: Router) {}
  @Input() dataSource: any;
  @Input() customerCode: any;
  arrayOfCustomerAssests = [];
  @Output() sendCustomerAsset = new EventEmitter();
  @Output() sendCustomerToSplice = new EventEmitter();
  customerAsset: CustomerAsset = { assetType: {} } as CustomerAsset;
  arrayOfAssetType = [];
  indexOfCustomerAssets: any;

  ngOnInit() {
    this.customerProvider.getCustomerTypes().subscribe((Res: any) => {
      this.arrayOfAssetType = Res.assetTypes;
      if (this.router.url !== '/pages/customer/add-customer') {
        this.customerAsset = this.dataSource;
        this.arrayOfAssetType.forEach((v, i ) => {
          if (v.assetTypeCode == this.customerAsset.assetType.assetTypeCode) {
           const el = document.getElementById("mySelect") as HTMLSelectElement; el.selectedIndex = i + 1;
          }
        });
      }
    });
  }

  onSelectAssetType(event) {
    const id = event.target.value;
    this.arrayOfAssetType.forEach(v => v.assetTypeCode == id ? this.customerAsset.assetType = v : "");
  }
  onPreesAdd() {
    this.customerAsset.customerCode = this.customerCode;
    this.customerAsset.companyCode = sessionStorage.getItem("companyCode");
    const newObj = JSON.parse(JSON.stringify(this.customerAsset));
    this.arrayOfCustomerAssests.push(newObj);
    this.sendCustomerAsset.emit(this.arrayOfCustomerAssests);
    this.resetAllValues();
  }
  onPressCancel() {
    this.resetAllValues();
  }
  onPressSpliceCustomer(i) {
    this.arrayOfCustomerAssests.splice(i, 1);
    this.sendCustomerAsset.emit(this.arrayOfCustomerAssests);
  }
  resetAllValues() {
    this.arrayOfCustomerAssests = JSON.parse(JSON.stringify(this.arrayOfCustomerAssests));
    this.customerAsset.comment = "";
    this.customerAsset.description = "";
    this.customerAsset.imageUrl = "";
    this.customerAsset.installationDate = "";
    this.customerAsset.installedBy = "";
    this.customerAsset.removalDate = "";
    this.customerAsset.removedBy = "";
    this.customerAsset.serialNo = "";
    const el = document.getElementById("mySelect") as HTMLSelectElement; el.selectedIndex = 0;

  }
  onPressEditCustomer(obj, i) {
    this.customerAsset = obj;
    this.indexOfCustomerAssets = i;
    this.arrayOfAssetType.forEach((v, i ) => {
      if (v.assetTypeCode == this.customerAsset.assetType.assetTypeCode) {
       const el = document.getElementById("mySelectEdit") as HTMLSelectElement; el.selectedIndex = i + 1;
      }
    });

  }
  onPreesEdit() {
    const newObj = JSON.parse(JSON.stringify(this.customerAsset));
    this.arrayOfCustomerAssests.splice(this.indexOfCustomerAssets, newObj);
    this.sendCustomerAsset.emit(this.arrayOfCustomerAssests);
  }

}
