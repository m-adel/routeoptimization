import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerAssetsEditComponent } from './customer-assets-edit.component';

describe('CustomerAssetsEditComponent', () => {
  let component: CustomerAssetsEditComponent;
  let fixture: ComponentFixture<CustomerAssetsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerAssetsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerAssetsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
