import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CustomerService } from '../../service/customer.service';
import { Router } from '@angular/router';
import { CustomerAsset } from '../../interface/customer-asset';
import { AddEditCustomerService } from '../../service/add-edit-customer.service';

@Component({
  selector: 'app-customer-assets-edit',
  templateUrl: './customer-assets-edit.component.html',
  styleUrls: ['./customer-assets-edit.component.sass']
})
export class CustomerAssetsEditComponent implements OnInit {
  constructor(
    private customerProvider: CustomerService,
    public router: Router,
    private addEditCustomerProvider: AddEditCustomerService
    ) {}
  @Input() dataSource: any;
  @Input() customerCode: any;
  arrayOfCustomerAssests = [];
  @Output() sendCustomerAsset = new EventEmitter();
  @Output() sendCustomerToSplice = new EventEmitter();
  customerAsset: CustomerAsset = { assetType: {} } as CustomerAsset;
  arrayOfAssetType = [];
  indexOfCustomerAssets: any;

  ngOnInit() {
    console.log(this.router.url);
    this.customerProvider.getCustomerTypes().subscribe((Res: any) => {
      this.arrayOfAssetType = Res.assetTypes;
      this.arrayOfCustomerAssests = this.dataSource;
    });
  }

  onSelectAssetType(event) {
    const id = event.target.value;
    this.arrayOfAssetType.forEach(v => v.assetTypeCode == id ? this.customerAsset.assetType = v : "");
  }
  onPreesAdd() {
    this.customerAsset.customerCode = this.customerCode;
    this.customerAsset.companyCode = sessionStorage.getItem("companyCode");
    const newObj = JSON.parse(JSON.stringify(this.customerAsset));
    this.addEditCustomerProvider.addSingleCustomerAssets(newObj).subscribe((Res) => {
      this.arrayOfCustomerAssests.push(Res);
      this.resetAllValues();
    });
  }
  onPressCancel() {
    this.resetAllValues();
  }
  onPressSpliceCustomer(id, i) {
    this.addEditCustomerProvider.deleteSingleCustomerAssets(id).subscribe((Res) => {
      this.arrayOfCustomerAssests.splice(i, 1);
    }, err => {
      if (err.error.text === "Entity deleted successfully") {
        this.arrayOfCustomerAssests.splice(i, 1);
      }
    });
  }
  resetAllValues() {
    this.arrayOfCustomerAssests = JSON.parse(JSON.stringify(this.arrayOfCustomerAssests));
    this.customerAsset.comment = "";
    this.customerAsset.description = "";
    this.customerAsset.imageUrl = "";
    this.customerAsset.installationDate = "";
    this.customerAsset.installedBy = "";
    this.customerAsset.removalDate = "";
    this.customerAsset.removedBy = "";
    this.customerAsset.serialNo = "";
    const el = document.getElementById("mySelect") as HTMLSelectElement; el.selectedIndex = 0;

  }
  onPressEditCustomer(obj, i) {
    this.customerAsset = obj;
    this.indexOfCustomerAssets = i;
    this.arrayOfAssetType.forEach((v, i ) => {
      if (v.assetTypeCode == this.customerAsset.assetType.assetTypeCode) {
       const el = document.getElementById("mySelectEdit") as HTMLSelectElement; el.selectedIndex = i + 1;
      }
    });
  }
  onPreesEdit() {
    const newObj = JSON.parse(JSON.stringify(this.customerAsset));
    this.arrayOfCustomerAssests.splice(this.indexOfCustomerAssets, newObj);
    this.addEditCustomerProvider.editCustomerAssets(newObj).subscribe((Res: any) => {
      this.arrayOfCustomerAssests.splice(this.indexOfCustomerAssets, Res);
    });
  }

}
