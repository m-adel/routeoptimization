import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CustomerSalesData } from '../../interface/customer-sales-data';
import { CustomerService } from '../../service/customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-seles-data',
  templateUrl: './customer-seles-data.component.html',
  styleUrls: ['./customer-seles-data.component.sass']
})
export class CustomerSelesDataComponent implements OnInit {
  customerSalesData: CustomerSalesData = {id: {}, customerType: {}} as CustomerSalesData;
  customersType = [];
  @Output() sendCustomerSeles = new EventEmitter();
  @Input() dataSource: any;
  constructor(private customerProvider: CustomerService, public router: Router) { }

  ngOnInit() {
    this.customerSalesData.id.companyCode = sessionStorage.getItem("companyCode");
    this.customerProvider.getCustomerTypes().subscribe((Res: any) => {
      this.customersType = Res.customerTypes;
      if (this.router.url !== '/pages/customer/add-customer') {
        this.customerSalesData = this.dataSource;
        this.customersType.forEach((v, i ) => {
          if (v.customerTypeCode == this.customerSalesData.customerType.customerTypeCode) {
           setTimeout(() => {
            const el = document.getElementById("selectDataSales") as HTMLSelectElement; el.selectedIndex = i + 1;
           }, 20);
          }
        });
      }
    });
  }

  onSelectCustomerType(event) {
    const id = event.target.value;
    this.customersType.forEach((v) => v.customerTypeCode == id ? this.customerSalesData.customerType = v : "");
    this.sendCustomerSeles.emit(this.customerSalesData);
  }
}
