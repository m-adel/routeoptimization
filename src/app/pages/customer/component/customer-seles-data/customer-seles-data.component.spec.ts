import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerSelesDataComponent } from './customer-seles-data.component';

describe('CustomerSelesDataComponent', () => {
  let component: CustomerSelesDataComponent;
  let fixture: ComponentFixture<CustomerSelesDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerSelesDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerSelesDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
