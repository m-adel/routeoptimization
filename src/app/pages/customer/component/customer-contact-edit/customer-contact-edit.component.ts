import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { CustomerContact } from '../../interface/customer-contact';
import { AddEditCustomerService } from '../../service/add-edit-customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-contact-edit',
  templateUrl: './customer-contact-edit.component.html',
  styleUrls: ['./customer-contact-edit.component.sass']
})
export class CustomerContactEditComponent implements OnInit, OnChanges {
  customerContact: CustomerContact = {} as CustomerContact;
  @Input() customerCode: any;
  @Output() sendCustomerContact = new  EventEmitter();
  @Output() sendCustomerToSplice = new EventEmitter();
  @Input() dataSource: any;
  arrayOfContacts = [];
  indexOfCustomerContact: any;
  constructor(private addEditCustomer: AddEditCustomerService, public router: Router) { }

  ngOnInit() {
    this.customerContact.active = false;
  }
  ngOnChanges() {
    if (this.dataSource) {
      this.arrayOfContacts = this.dataSource;
    }
  }
  onPressAdd() {
    this.customerContact.customerCode = this.customerCode;
    this.customerContact.companyCode = sessionStorage.getItem("companyCode");
    const newObj = JSON.parse(JSON.stringify(this.customerContact));
    this.addEditCustomer.addCustomerSingleContact(newObj).subscribe((Res) => {
      this.arrayOfContacts.push(Res);
      this.resetAllValues();
    });
  }
  resetAllValues() {
    this.arrayOfContacts = JSON.parse(JSON.stringify(this.arrayOfContacts));
    this.customerContact.active = false;
    this.customerContact.mobile = "";
    this.customerContact.name = "";
    this.customerContact.telephone = "";
    this.customerContact.title = "";
  }
  onPressSpliceArrayOfContacts(id, i) {
    this.addEditCustomer.deleteCustomerContact(id).subscribe((Res) => {
      this.arrayOfContacts.splice(i, 1);
    }, err => {
      if (err.error.text === "Entity deleted successfully") {
        this.arrayOfContacts.splice(i, 1);
      }
    });
  }
  onPressEditContact(obj, i) {
    this.customerContact = obj;
    this.indexOfCustomerContact = i;
  }
  onPressEdit() {
    const newObj = JSON.parse(JSON.stringify(this.customerContact));
    this.addEditCustomer.editCustomerContact(newObj).subscribe((Res) => {
      this.arrayOfContacts.splice(this.indexOfCustomerContact, 1, Res);
    });
  }

}
