import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerContactEditComponent } from './customer-contact-edit.component';

describe('CustomerContactEditComponent', () => {
  let component: CustomerContactEditComponent;
  let fixture: ComponentFixture<CustomerContactEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerContactEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerContactEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
