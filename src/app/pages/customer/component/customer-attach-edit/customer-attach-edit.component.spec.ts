import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerAttachEditComponent } from './customer-attach-edit.component';

describe('CustomerAttachEditComponent', () => {
  let component: CustomerAttachEditComponent;
  let fixture: ComponentFixture<CustomerAttachEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerAttachEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerAttachEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
