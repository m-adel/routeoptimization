import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CustomerAttach } from '../../interface/customer-attach';
import { Router } from '@angular/router';
import { CustomerService } from '../../service/customer.service';
import { AddEditCustomerService } from '../../service/add-edit-customer.service';

@Component({
  selector: 'app-customer-attach-edit',
  templateUrl: './customer-attach-edit.component.html',
  styleUrls: ['./customer-attach-edit.component.sass']
})
export class CustomerAttachEditComponent implements OnInit {
  displayImage = false;
  Imagefile: any;
  arrayOfAttachmentTypes = [];
  customerAttach: CustomerAttach = {attachmentType: {}} as CustomerAttach;
  success = false;
  uploadBtnVisabiltiy = false;
  Responsemessage = "";
  error = false;
  arrayOfImages = [];
  indexOfCustomerAttach: any;
  @Input() customerCode: any;
  @Output() sendCustomerAttach  = new EventEmitter();
  @Output() sendCustomerToSplice = new EventEmitter();
  @Input() dataSource: any;
  constructor(
    public router: Router,
    private customerProvider: CustomerService,
    private addEditCustomerProvider: AddEditCustomerService
    ) {}

  ngOnInit() {
    this.customerProvider.getAttachmentType().subscribe((Res: any) => {
      this.arrayOfAttachmentTypes = Res;
      this.arrayOfImages = this.dataSource;
    });
  }
  onPressAddNewImage() {
    this.displayImage = false;
    this.resetAllValues();
  }

  onChooseImage(event) {
    this.displayImage = true;
    this.uploadBtnVisabiltiy = true;
    this.Imagefile = event.target.files;
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e: any) => { this.customerAttach.attachmentUrl = e.target.result; };
    }
  }
  CancelImage() {
    this.Imagefile = null;
    this.displayImage = false;
    this.uploadBtnVisabiltiy = false;
  }

  onSelectType(event) {
    const id = event.target.value;
    this.arrayOfAttachmentTypes.forEach(v => v.attachmentTypeCode == id ? this.customerAttach.attachmentType = v : "");
  }
  onPressUpload() {
    this.customerAttach.customerCode = this.customerCode;
    const formData = new FormData();
    for (const file of this.Imagefile) { formData.append("file", file, file.name); }
    this.customerProvider.uploadFile(formData).subscribe((Res: any) => {
      const newObj = JSON.parse(JSON.stringify(this.customerAttach));
      newObj.attachmentUrl = Res[0].value;
      this.addNewAttachment(newObj);
      this.uploadBtnVisabiltiy = false;
      this.success = true;
      this.Responsemessage = "Uploaded Successfully !";
      this.resetAllValues();
      setTimeout(() => this.success = false, 2000);
    }, err => {
      this.error = true;
      this.Responsemessage = err.error.message;
      setTimeout(() => this.error = false, 2000);
    });
  }
  onPressSpliceImageUploaded(id, i) {
    this.addEditCustomerProvider.deleteCustomerAttachment(id).subscribe((Res: any) => {
      this.arrayOfImages.splice(i, 1);
      this.success  = true;
      this.error = false;
      this.Responsemessage = "Succussfully Deleted !";
      setTimeout(() => this.success = false, 2000);
    }, err => {
      if (err.error.text === "Entity deleted successfully") {
        this.arrayOfImages.splice(i, 1);
        this.success  = true;
        this.Responsemessage = "Succussfully Deleted !";
        setTimeout(() => this.success = false, 2000);
      } else {
        this.error = true;
        this.Responsemessage = err.error.message;
      }
    });
  }
  resetAllValues() {
      this.arrayOfImages = JSON.parse(JSON.stringify(this.arrayOfImages));
      this.customerAttach.comment = "";
      this.customerAttach.attachmentUrl = "";
      const ele = document.getElementById("SelectAttach") as HTMLSelectElement;
      ele.selectedIndex = 0;
      this.Imagefile = null;
      this.displayImage = false;
      this.uploadBtnVisabiltiy = false;
  }
  onPressEditImage(obj, i) {
    this.customerAttach = obj;
    this.displayImage = true;
    this.indexOfCustomerAttach = i;
    this.arrayOfAttachmentTypes.forEach((v, index ) => {
      if (v.attachmentTypeCode == obj.attachmentType.attachmentTypeCode) {
       setTimeout(() => {
        const el = document.getElementById("SelectAttachEditing") as HTMLSelectElement; el.selectedIndex = index + 1;
       }, 10);
      }
    });
  }
  onPressUploadEditing() {
    const newObj = JSON.parse(JSON.stringify(this.customerAttach));
    if (this.Imagefile) {
    const formData = new FormData();
    for (const file of this.Imagefile) { formData.append("file", file, file.name); }
    this.customerProvider.uploadFile(formData).subscribe((Res: any) => {
      newObj.attachmentUrl = Res[0].value;
      this.editAttachment(newObj);
      this.uploadBtnVisabiltiy = false;
      this.success = true;
      this.Responsemessage = "Uploaded Successfully !";

      this.resetAllValues();
      setTimeout(() => this.success = false, 2000);
    }, err => {
      this.error = true;
      this.Responsemessage = err.error.message;
      setTimeout(() => this.error = false, 2000);
      this.resetAllValues();
    });
    } else {
      this.editAttachment(newObj);
    }
  }
  addNewAttachment(newObj) {
    this.addEditCustomerProvider.addSingleCustomerAttachment(newObj).subscribe((Res: any) => {
      this.arrayOfImages.push(Res);
    });
  }
  editAttachment(newObj) {
    this.addEditCustomerProvider.editCustomerAttachment(newObj).subscribe((Res: any) => {
      this.arrayOfImages.splice(this.indexOfCustomerAttach, 1, Res);
    });
  }
}
