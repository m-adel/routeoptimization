import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerAttachComponent } from './customer-attach.component';

describe('CustomerAttachComponent', () => {
  let component: CustomerAttachComponent;
  let fixture: ComponentFixture<CustomerAttachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerAttachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerAttachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
