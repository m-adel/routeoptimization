import { Component, OnInit, Output , EventEmitter, Input} from "@angular/core";
import { CustomerAttach } from '../../interface/customer-attach';
import { CustomerService } from '../../service/customer.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-customer-attach",
  templateUrl: "./customer-attach.component.html",
  styleUrls: ["./customer-attach.component.sass"]
})
export class CustomerAttachComponent implements OnInit {
  displayImage = false;
  Imagefile: any;
  arrayOfAttachmentTypes = [];
  customerAttach: CustomerAttach = {attachmentType: {}} as CustomerAttach;
  success = false;
  uploadBtnVisabiltiy = false;
  Responsemessage = "";
  error = false;
  arrayOfImages = [];
  indexOfCustomerAttach: any;
  @Input() customerCode: any;
  @Output() sendCustomerAttach  = new EventEmitter();
  @Output() sendCustomerToSplice = new EventEmitter();
  @Input() dataSource: any;
  constructor(public router: Router , private customerProvider: CustomerService) {}

  ngOnInit() {
    this.customerProvider.getAttachmentType().subscribe((Res: any) => {
      this.arrayOfAttachmentTypes = Res;
      if (this.router.url !== '/pages/customer/add-customer') {
        this.customerAttach = this.dataSource;
        this.arrayOfAttachmentTypes.forEach((v, i ) => {
          if (v.attachmentTypeCode == this.customerAttach.attachmentType.attachmentTypeCode) {
           const el = document.getElementById("SelectAttach") as HTMLSelectElement; el.selectedIndex = i + 1;
           this.displayImage = true;
          }
        });
      }
    });
  }
  onPressAddNewImage() {
    this.displayImage = false;
    this.resetAllValues();
  }

  onChooseImage(event) {
    this.displayImage = true;
    this.uploadBtnVisabiltiy = true;
    this.Imagefile = event.target.files;
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e: any) => { this.customerAttach.attachmentUrl = e.target.result; };
    }
  }
  CancelImage() {
    this.Imagefile = null;
    this.displayImage = false;
    this.uploadBtnVisabiltiy = false;
  }

  onSelectType(event) {
    const id = event.target.value;
    this.arrayOfAttachmentTypes.forEach(v => v.attachmentTypeCode == id ? this.customerAttach.attachmentType = v : "");
  }
  onPressUpload() {
    this.customerAttach.customerCode = this.customerCode;
    const formData = new FormData();
    for (const file of this.Imagefile) { formData.append("file", file, file.name); }
    this.customerProvider.uploadFile(formData).subscribe((Res: any) => {
      const newObj = JSON.parse(JSON.stringify(this.customerAttach));
      newObj.attachmentUrl = Res[0].value;
      this.arrayOfImages.push(newObj);
      this.uploadBtnVisabiltiy = false;
      this.success = true;
      this.Responsemessage = "Uploaded Successfully !";
      this.sendCustomerAttach.emit(this.arrayOfImages);
      this.resetAllValues();
      setTimeout(() => this.success = false, 2000);
    }, err => {
      this.error = true;
      this.Responsemessage = err.error.message;
      setTimeout(() => this.error = false, 2000);
    });
  }
  onPressSpliceImageUploaded(i) {
    this.arrayOfImages.splice(i, 1);
    this.sendCustomerAttach.emit(this.arrayOfImages);
  }
  resetAllValues() {
      this.arrayOfImages = JSON.parse(JSON.stringify(this.arrayOfImages));
      this.customerAttach.comment = "";
      this.customerAttach.attachmentUrl = "";
      const ele = document.getElementById("SelectAttach") as HTMLSelectElement;
      ele.selectedIndex = 0;
      this.Imagefile = null;
      this.displayImage = false;
      this.uploadBtnVisabiltiy = false;
  }
  onPressEditImage(obj, i) {
    this.customerAttach = obj;
    this.displayImage = true;
    this.indexOfCustomerAttach = i;
    this.arrayOfAttachmentTypes.forEach((v, index ) => {
      if (v.attachmentTypeCode == obj.attachmentType.attachmentTypeCode) {
       setTimeout(() => {
        const el = document.getElementById("SelectAttachEditing") as HTMLSelectElement; el.selectedIndex = index + 1;
       }, 10);
      }
    });
  }
  onPressUploadEditing() {
    const newObj = JSON.parse(JSON.stringify(this.customerAttach));
    const formData = new FormData();
    for (const file of this.Imagefile) { formData.append("file", file, file.name); }
    this.customerProvider.uploadFile(formData).subscribe((Res: any) => {
      newObj.attachmentUrl = Res[0].value;
      this.arrayOfImages.splice(this.indexOfCustomerAttach, 1, newObj );
      this.sendCustomerAttach.emit(this.arrayOfImages);
      this.uploadBtnVisabiltiy = false;
      this.success = true;
      this.Responsemessage = "Uploaded Successfully !";
      this.sendCustomerAttach.emit(this.arrayOfImages);
      this.resetAllValues();
      setTimeout(() => this.success = false, 2000);
    }, err => {
      this.error = true;
      this.Responsemessage = err.error.message;
      setTimeout(() => this.error = false, 2000);
    });
    this.resetAllValues();
  }
}
