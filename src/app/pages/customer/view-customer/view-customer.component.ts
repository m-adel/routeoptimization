import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerService } from '../service/customer.service';
import { AddEditCustomerService } from '../service/add-edit-customer.service';
import { Customer } from '../interface/customer';
import { CustomerSalesData } from '../interface/customer-sales-data';

@Component({
  selector: 'app-view-customer',
  templateUrl: './view-customer.component.html',
  styleUrls: ['./view-customer.component.sass']
})
export class ViewCustomerComponent implements OnInit {
  customer: Customer = {} as Customer;
  customerSalesData: CustomerSalesData = {id: {} } as CustomerSalesData;
  customerAsset = [];
  customerContact = [];
  customerAttach = [];
  countries = [];
  countriesNames = [];
  industries = [];
  industriesNames = [];
  attachTypes = [];
  regions = [];
  regionsNames = [];
  goves = [];
  goverNames = [];
  customerCode: any;
  success = false;
  Responsemessage = "";
  error = false;
  constructor(
    public router: Router,
    private customerProvider: CustomerService,
    private editCustomerProvider: AddEditCustomerService,
    public activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.customerCode = this.activeRoute.snapshot.params.id;
    this.customerProvider.getCustomerDetails(this.customerCode).subscribe((Res: any) => {
      this.customer = Res.customerSalesData.customer;
      this.customerAsset = Res.customerAssets;
      this.customerContact = Res.customerContacts;
      this.customerSalesData.id = Res.customerSalesData.id;
      this.customerSalesData.customerType = Res.customerSalesData.customerType;
    });
    this.getLookUps();
    this.getGovernoments();
    this.getRegion();
    this.getAttachments();
  }
  getAttachments = () => {
    this.customerProvider.getCustomerAttachment(this.customerCode).subscribe((Res: any) => {
      if (Res.customerAttachments.length > 0) {
        this.customerAttach = Res.customerAttachments;
      }
    });
  }
  getLookUps = () => {
    this.customerProvider.getCustomerLookUps().subscribe((Res: any) => {
      this.countries = Res.countries;
      this.countriesNames = Res.countries.map(v => v.countryName);
      this.industries = Res.industries;
      this.industriesNames = Res.industries.map(v => v.industryName);
      this.attachTypes = Res.attachmentTypes;
    });
  }
  getGovernoments = () => {
    this.customerProvider.getGovernoments().subscribe((Res: any) => {
      this.goverNames = Res.map(v => v.governorateName);
      this.goves = Res;
    });
  }
  getRegion = () => {
    this.customerProvider.getRegions().subscribe((Res: any) => {
      this.regions = Res;
      this.regionsNames = Res.map(v => v.regionName);
    });
  }
  onChange = event => {
    const value = event.selectedData.toUpperCase();
    switch (event.DataFrom) {
      case "country": this.countries.forEach(v => v.countryName.toUpperCase() == value ? (this.customer.country = v) : "" ); break;
      case "industry": this.industries.forEach(v => v.industryName.toUpperCase() == value ? (this.customer.industry = v) : "" ); break;
      case "gover": this.goves.forEach(v => v.governorateName.toUpperCase() == value ? (this.customer.governorate = v) : "" ); break;
      case "region": this.regions.forEach(v => v.regionName.toUpperCase() == value ? (this.customer.region = v) : ""); break;
      default: break;
    }
  }


  onPressCancel() {
    this.router.navigateByUrl("/pages/customer/all-customers");
  }
}
