import { AllCustomerComponent } from './all-customer/all-customer.component';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerComponent } from "./customer.component";
import { ViewCustomerComponent } from './view-customer/view-customer.component';

const routes: Routes = [
    {
        path: '', component: CustomerComponent, children: [
            {
                path: 'add-customer', component: AddCustomerComponent
            },
            {
                path: "edit-customer/:id", component: EditCustomerComponent
            }, {
              path: "view-customer/:id", component: ViewCustomerComponent
          },
            {
                path: "all-customers", component: AllCustomerComponent
            },
            {
              path: '',
              redirectTo: '/pages/customer/all-customers',
              pathMatch: 'full'
          }
        ]
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CustomerRoutingModule { }
