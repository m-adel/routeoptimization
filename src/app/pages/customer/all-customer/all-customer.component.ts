import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../service/customer.service';

@Component({
  selector: 'app-all-customer',
  templateUrl: './all-customer.component.html',
  styleUrls: ['./all-customer.component.sass']
})
export class AllCustomerComponent implements OnInit {
  data = [];
  originalData = [];
  page: any = 1;
  pageSize: any = 10;
  totalItems: any = 0;
  offset = 0;
  Responsemessage = "";
  error = false;
  seaching = false;
  loaded = true;
  customerCode: any;
  indexOfcustomerCode: any;
  constructor(public router: Router, private customerProvider: CustomerService) { }

  ngOnInit() {
    this.getCountOfCustomers();
  }
  getCountOfCustomers = () => {
    this.customerProvider.getCountOfCustomerForCompany().subscribe((Res: any) => {
      this.totalItems = Res;
      this.getAllCustomers();
    });
  }
  getAllCustomers = () => {
    this.customerProvider.getAllCustomerForSingleCompany(this.offset, this.pageSize).subscribe((Res: any) => {
      this.data = Res;
      this.loaded = false;
      this.seaching = false;

    });
  }
  PageChange(e) {
    this.loaded = true;
    this.page = e - 1;
    this.offset = this.page * this.pageSize;
    this.ngOnInit();
  }
  onChange(e) {
    this.offset = 0;
    const customerName = e.target.value;
    if (customerName == "") {
      this.seaching = false;
      this.ngOnInit();
    } else {
      this.customerProvider.searchCustomerByName(customerName, this.totalItems).subscribe((Res: any) => {
          this.data = Res;
          this.seaching = true;
          if (customerName == "") {
            this.seaching = false;
            this.ngOnInit();
          }
      });
    }
  }
  onPressEditIcon(code) {
    this.router.navigate(["/pages/customer/edit-customer", code]);
  }
  onPressTr(code) {
    this.router.navigate(["/pages/customer/view-customer", code]);
  }
  onPressDeleteIcon(code, index) {
    this.customerCode = code;
    this.indexOfcustomerCode = index;
  }
  handelDeleteCustomer() {
    this.customerProvider.deletCustomer(this.customerCode).subscribe((Res: any) => {
    }, err => {
      if (err.error.text === "Entity deleted successfully") {
        this.data.splice(this.indexOfcustomerCode, 1);
        this.Responsemessage = "Deleted Successfully";
        this.error = true;
        setTimeout(() => this.error = false, 2000);
      } else {
        this.Responsemessage = "This Customer Has Already Use";
        this.error = true;
        setTimeout(() => this.error = false, 4000);
      }
    });
  }
}
