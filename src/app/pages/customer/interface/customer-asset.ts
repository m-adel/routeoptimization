export interface CustomerAsset {
  assetId: any;
  active: boolean;
  comment: string;
  companyCode: string;
  customerCode: string;
  description: string;
  imageUrl: string;
  installationDate: string;
  installedBy: string;
  removalDate: string;
  removedBy: string;
  serialNo: string;
  assetType: AssetType;
}
interface AssetType {
  assetTypeCode: string;
  assetTypeName: string;
}
