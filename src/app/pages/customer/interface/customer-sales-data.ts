export interface CustomerSalesData {
  id: Id;
  customerType: {
    customerTypeCode: string;
    customerTypeColor: string;
    customerTypeName: string;
    pinScheduledUrl: string;
    pinUrl: string;
  };
}
interface Id {
  customerCode: string;
  companyCode: string;
}
