import { Country, Governoment, Region } from '../../root/interface/territory';

export interface Customer {
  customerCode: string;
  active: boolean;
  address: string;
  latitude: any;
  longitude: any;
  mobile: string;
  name1: string;
  postalCode: string;
  telephone: string;
  country: Country;
  governorate: Governoment;
  region: Region;
  industry: Industry;
}
interface Industry {
  industryCode: string;
  industryName: string;
}
