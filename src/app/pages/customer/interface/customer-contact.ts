export interface CustomerContact {
  contactId: any;
  active: boolean;
  companyCode: string;
  customerCode: string;
  mobile: string;
  name: string;
  telephone: string;
  title: string;
}
