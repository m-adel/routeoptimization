export interface CustomerAttach {
  attachmentId: any;
  attachmentUrl: string;
  comment: string;
  customerCode: string;
  attachmentType: {
    attachmentTypeCode: string;
    attachmentTypeName: string;
  };
}
