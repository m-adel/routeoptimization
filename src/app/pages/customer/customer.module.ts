import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllCustomerComponent } from './all-customer/all-customer.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { CustomerComponent } from "./customer.component";
import { CustomerRoutingModule } from "./customer.routing";
import { SharedModule } from 'src/app/shared/shared.module';
import { AgmCoreModule } from '@agm/core';
import { CustomerMapComponent } from './component/customer-map/customer-map.component';
import { CustomerAttachComponent } from './component/customer-attach/customer-attach.component';
import { CustomerContactComponent } from './component/customer-contact/customer-contact.component';
import { CustomerSelesDataComponent } from './component/customer-seles-data/customer-seles-data.component';
import { CustomerAssetsComponent } from './component/customer-assets/customer-assets.component';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { CustomerAssetsEditComponent } from './component/customer-assets-edit/customer-assets-edit.component';
import { CustomerAttachEditComponent } from './component/customer-attach-edit/customer-attach-edit.component';
import { CustomerContactEditComponent } from './component/customer-contact-edit/customer-contact-edit.component';
import { ViewCustomerComponent } from './view-customer/view-customer.component';

@NgModule({
  declarations: [
    AllCustomerComponent,
    AddCustomerComponent,
    EditCustomerComponent,
    CustomerComponent,
    CustomerMapComponent,
    CustomerAttachComponent,
    CustomerContactComponent,
    CustomerSelesDataComponent,
    CustomerAssetsComponent,
    CustomerAssetsEditComponent,
    CustomerAttachEditComponent,
    CustomerContactEditComponent,
    ViewCustomerComponent
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    FormsModule,
    SharedModule,
    AgmCoreModule,
    ReactiveFormsModule,
    UiSwitchModule
  ]
})
export class CustomerModule { }
