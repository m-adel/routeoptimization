import { Component } from "@angular/core";

@Component({
  selector: "app-customer",
  template: `
  <!-- <app-child-navs [navs]="navs"></app-child-navs> -->

    <router-outlet></router-outlet>
  `
})
export class CustomerComponent {
  navs = [
    { name: "All Customers", link: "/pages/customer/all-customers" },
    { name: "Add Customer", link: "/pages/customer/add-customer" },
  ];
}
