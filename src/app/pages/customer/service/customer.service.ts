import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebServicesService } from 'src/app/providers/web-services.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(public http: HttpClient, public config: WebServicesService) { }
  getCountOfCustomerForCompany = () => {
    const url = this.config.serviceUrl + this.config.allCustomerCountForCompany.
    replace("{companyCode}", sessionStorage.getItem("companyCode"));
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getAllCustomerForSingleCompany = (offset, limit) => {
    const url = this.config.serviceUrl + this.config.getAllCustomers.
    replace('{0}', sessionStorage.getItem("companyCode")).
    replace("{1}", offset).replace("{2}", limit);
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  searchCustomerByName = (name, limit) => {
    const url = this.config.serviceUrl + this.config.searchByCustomerName.replace("{1}", name)
    .replace("{2}", sessionStorage.getItem("companyCode")).replace("{3}", limit);
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getCustomerLookUps() {
    const url = this.config.serviceUrl + this.config.getCustomerLookUps;
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getGovernoments() {
    const url = this.config.serviceUrl + this.config.getAllGovernoment;
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getRegions() {
    const url = this.config.serviceUrl + this.config.getAllRegions;
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getCustomerTypes() {
    const url = this.config.serviceUrl + this.config.getCutomerTypeLookUps;
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }

  getAttachmentType() {
    const url = this.config.serviceUrl + this.config.getAttachmentType;
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  uploadFile(data) {
    const url = this.config.serviceUrl + this.config.uploadFile;
    const header = this.config.headers;
    return this.http.post(url, data, {headers: header});
  }
  deletCustomer(code) {
    const url = this.config.serviceUrl + this.config.deleteCustomer.replace("{customerCode}", code)
    const header = this.config.headers;
    return this.http.delete(url, {headers: header});
  }
  getCustomerDetails(code) {
    const url = this.config.serviceUrl + this.config.customerDetails.replace("{customerCode}", code).
                replace("{companyCode}", sessionStorage.getItem("companyCode"));
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getCustomerAttachment(code) {
    const url = this.config.serviceUrl + this.config.customerAttachDetails.replace("{customerCode}", code);
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
}
