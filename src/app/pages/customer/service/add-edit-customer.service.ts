import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { WebServicesService } from "src/app/providers/web-services.service";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class AddEditCustomerService {
  constructor(public http: HttpClient, public config: WebServicesService) {}
  addCustomer(data) {
    const url =
      this.config.serviceUrl +
      this.config.addCustomer.replace("{companyCode}", sessionStorage.getItem("companyCode"));
    const header = this.config.headers;
    return this.http.post(url, data, {headers: header});
  }
  editCustomer(data) {
    const url = this.config.serviceUrl + this.config.editCustomer;
    const header = this.config.headers;
    return this.http.put(url, data, {headers: header});
  }
  addCustomerSalesData(data) {
    const url = this.config.serviceUrl + this.config.addCustomerDataSales;
    const header = this.config.headers;
    return this.http.put(url, data, {headers: header});
  }
  addCustomerAttachment(data) {
    const arrayOfData = [];
    data.forEach(element => {
      const url = this.config.serviceUrl + this.config.addCustomerAttach;
      const header = this.config.headers;
      arrayOfData.push(this.http.post(url, element, {headers: header}));
    });
    return Observable.forkJoin(arrayOfData);
  }
  addSingleCustomerAttachment(data) {
    const url = this.config.serviceUrl + this.config.addCustomerAttach;
    const header = this.config.headers;
    return this.http.post(url, data, {headers: header});
  }
  editCustomerAttachment(data) {
    const url = this.config.serviceUrl + this.config.addCustomerAttach;
    const header = this.config.headers;
    return this.http.put(url, data, {headers: header});
  }
  deleteCustomerAttachment(id) {
    const url = this.config.serviceUrl + this.config.deleteCustomerAttachment.replace("{attachmentId}", id);
    const header = this.config.headers;
    return this.http.delete(url, {headers: header});
  }
  addCustomerAssets(data) {
    const arrayOfData = [];
    data.forEach(element => {
      const url = this.config.serviceUrl + this.config.addCutomerAssets;
      const header = this.config.headers;
      arrayOfData.push(this.http.post(url, element, {headers: header}));
    });
    return Observable.forkJoin(arrayOfData);
  }
  addSingleCustomerAssets(data) {
    const url = this.config.serviceUrl + this.config.addCutomerAssets;
    const header = this.config.headers;
    return this.http.post(url, data, {headers: header});
  }
  deleteSingleCustomerAssets(id) {
    const url = this.config.serviceUrl + this.config.deleteCustomerAssets.replace("{assetId}", id);
    const header = this.config.headers;
    return this.http.delete(url, {headers: header});
  }
  editCustomerAssets(data) {
    const url = this.config.serviceUrl + this.config.addCutomerAssets;
    const header = this.config.headers;
    return this.http.put(url, data, {headers: header});
  }
  addCustomerContact(data) {
    const arrayOfData = [];
    data.forEach(element => {
      const url = this.config.serviceUrl + this.config.addCustomerContact;
      const header = this.config.headers;
      arrayOfData.push(this.http.post(url, element, {headers: header}));
    });
    return Observable.forkJoin(arrayOfData);
  }
  addCustomerSingleContact(data) {
    const url = this.config.serviceUrl + this.config.addCustomerContact;
    const header = this.config.headers;
    return this.http.post(url, data, {headers: header});
  }
  deleteCustomerContact(id) {
    const url = this.config.serviceUrl + this.config.deleteCustomerContact.replace("{contactId}", id);
    const header = this.config.headers;
    return this.http.delete(url, {headers: header});
  }
  editCustomerContact(data) {
    const url = this.config.serviceUrl + this.config.addCustomerContact;
    const header = this.config.headers;
    return this.http.put(url, data, {headers: header});
  }
  allCountOfCustomer() {
    const url = this.config.serviceUrl + this.config.allCustomerCount;
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getLastCountOfCustomer(offset) {
    const url = this.config.serviceUrl + this.config.getAllCustomers.replace("{0}", "").
    replace("{1}", offset).replace("{2}", "1");
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
}
