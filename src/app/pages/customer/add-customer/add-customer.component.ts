import { Component, OnInit } from "@angular/core";
import { Customer } from "../interface/customer";
import { CustomerSalesData } from "../interface/customer-sales-data";
import { CustomerAsset } from "../interface/customer-asset";
import { CustomerContact } from "../interface/customer-contact";
import { CustomerAttach } from "../interface/customer-attach";
import { Router } from "@angular/router";
import { CustomerService } from "../service/customer.service";
import { AddEditCustomerService } from "../service/add-edit-customer.service";

@Component({
  selector: "app-add-customer",
  templateUrl: "./add-customer.component.html",
  styleUrls: ["./add-customer.component.sass"]
})
export class AddCustomerComponent implements OnInit {
  customer: Customer = {} as Customer;
  customerSalesData: CustomerSalesData = {id: {} } as CustomerSalesData;
  customerAsset = [];
  customerContact = [];
  customerAttach = [];
  countries = [];
  countriesNames = [];
  industries = [];
  industriesNames = [];
  attachTypes = [];
  regions = [];
  regionsNames = [];
  goves = [];
  goverNames = [];
  success = false;
  Responsemessage = "";
  error = false;
  constructor(
    public router: Router,
    private customerProvider: CustomerService,
    private addCustomerProvider: AddEditCustomerService
  ) {}
  ngOnInit() {
    this.getLookUps();
    this.getGovernoments();
    this.getRegion();
    this.getLastCustomerCode();
  }
  getLastCustomerCode = () => {
    this.addCustomerProvider.allCountOfCustomer().subscribe((Res: any) => {
      if (Res >  0 ) {
        this.addCustomerProvider.getLastCountOfCustomer( Res - 1 ).subscribe((Resp) => {
          const lastCustomerCode = (+Resp[0].customerCode + + 1).toString();
          const length = 10 - lastCustomerCode.toString().split("").length;
          const newArr = [];
          for (let index = 0; index < length; index++) {
            newArr.push("0");
          }
          this.customer.customerCode = newArr.join("") + lastCustomerCode;
        });
      }  else {
        this.customer.customerCode = "0000100100";
      }

    });
  }
  getLookUps = () => {
    this.customerProvider.getCustomerLookUps().subscribe((Res: any) => {
      this.countries = Res.countries;
      this.countriesNames = Res.countries.map(v => v.countryName);
      this.industries = Res.industries;
      this.industriesNames = Res.industries.map(v => v.industryName);
      this.attachTypes = Res.attachmentTypes;
      this.countries.map(v => v.countryName === "Egypt" ? this.customer.country = v : "");
    });
  }
  getGovernoments = () => {
    this.customerProvider.getGovernoments().subscribe((Res: any) => {
      this.goverNames = Res.map(v => v.governorateName);
      this.goves = Res;
    });
  }
  getRegion = () => {
    this.customerProvider.getRegions().subscribe((Res: any) => {
      this.regions = Res;
      this.regionsNames = Res.map(v => v.regionName);
    });
  }
  onChange = event => {
    const value = event.selectedData.toUpperCase();
    switch (event.DataFrom) {
      case "country": this.countries.forEach(v => v.countryName.toUpperCase() == value ? (this.customer.country = v) : "" ); break;
      case "industry": this.industries.forEach(v => v.industryName.toUpperCase() == value ? (this.customer.industry = v) : "" ); break;
      case "gover": this.goves.forEach(v => v.governorateName.toUpperCase() == value ? (this.customer.governorate = v) : "" ); break;
      case "region": this.regions.forEach(v => v.regionName.toUpperCase() == value ? (this.customer.region = v) : ""); break;
      default: break;
    }
  }
  OnReciveLatLng = event => {
    this.customer.latitude = event.lat;
    this.customer.longitude = event.lng;
  }
  ResviceData(event, from) {
    switch (from) {
      case "customerSalesData": this.customerSalesData = event; break;
      case "customerAttach": this.customerAttach = event; break;
      case "customerAsset": this.customerAsset = event; break;
      case "customerContact": this.customerContact = event; break;
      default: break;
    }
  }

  onPressSave() {
    this.addCustomerProvider.addCustomer(this.customer).subscribe((Res: any) => {
      if (this.customerAsset.length > 0) { this.addAssets(); }
      if (this.customerAttach.length > 0) { this.addCustomerAttach(); }
      if (this.customerContact.length > 0) { this.addCustomerContact(); }
      if (this.customerSalesData) { this.addCustomerSalesData(); }
      this.success = true;
      this.error = false;
      this.Responsemessage = "Successfully !";
      setTimeout(() => {
        this.success = false;
        this.router.navigateByUrl("pages/customer/all-customers");
      }, 3000);
    },err => {
      this.error = true;
      this.Responsemessage = "You must fill all fields"
      setTimeout(()=>{
          this.error = false
      },3000)

    });
  }
  addAssets() {
    this.addCustomerProvider.addCustomerAssets(this.customerAsset).subscribe((Res: any) => {});
  }
  addCustomerAttach() {
    this.addCustomerProvider.addCustomerAttachment(this.customerAttach).subscribe((Res) => {});
  }
  addCustomerContact() {
    this.addCustomerProvider.addCustomerContact(this.customerContact).subscribe((Res) => {});
  }
  addCustomerSalesData() {
    this.customerSalesData.id.customerCode = this.customer.customerCode;
    this.customerSalesData.id.companyCode = sessionStorage.getItem("companyCode");
    this.addCustomerProvider.addCustomerSalesData(this.customerSalesData).subscribe((Res) => {
    });
  }
  onPressCancel() {
    this.router.navigateByUrl("/pages/customer/all-customers");
  }
}
