import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.sass']
})
export class PageComponent implements OnInit {
accessToken: any;
companyName: any;
  constructor(public router: Router) { }

  ngOnInit() {
    sessionStorage.getItem("companyName") ? this.companyName = sessionStorage.getItem("companyName") : '';
    this.accessToken = sessionStorage.getItem("accessToken");
  }

}
