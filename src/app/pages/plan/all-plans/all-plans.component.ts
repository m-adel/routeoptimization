import { Component, OnInit } from "@angular/core";
import { PlanService } from '../service/plan.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-all-plans",
  templateUrl: "./all-plans.component.html",
  styleUrls: ["./all-plans.component.sass"]
})
export class AllPlansComponent implements OnInit {
  currentDate: any;

  constructor(public planProvider: PlanService, public router: Router) {}
  data = [];
  today: any;
  planId:any
  index:any
  arrayOfTerritoryCodes = [];
  ngOnInit() {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, "0");
    const mm = String(today.getMonth() + 1).padStart(2, "0");
    const yyyy = today.getFullYear();
    this.today = `${yyyy}-${mm}-${dd}`;
    this.planProvider.getCustomersVistsByDate(this.today).subscribe((Res: any) => {
      this.data = Res;
      console.log(this.data);

      this.data  = this.data.reduce((unique, o) => {
        if (!unique.some(obj => obj.id.territoryCode  === o.id.territoryCode)) {
          unique.push(o);
        }
        return unique;
      }, []);
      this.arrayOfTerritoryCodes = this.data.map(v => v.id.territoryCode);
      this.getTerritoryName();
    });
  }
  handleChangeDate(date) {
    this.currentDate = "";
    this.currentDate = date;
    this.planProvider.getCustomersVistsByDate(date).subscribe((Res: any) => {
      this.data = Res;
      this.data  = this.data.reduce((unique, o) => {
        if (!unique.some(obj => obj.id.territoryCode  === o.id.territoryCode)) {
          unique.push(o);
        }
        return unique;
    }, []);
      this.arrayOfTerritoryCodes = this.data.map(v => v.id.territoryCode);
      this.getTerritoryName();
    });
  }
  getTerritoryName() {
    this.planProvider.getArrayOfTerritoryName(this.arrayOfTerritoryCodes).subscribe((Res: any) => {
      this.data.forEach(v => Res.forEach(element => {
        if (v.id.territoryCode == element.territory.territoryCode) {
          v.territoryName = element.territory.name1;
        }
      }));
    });
  }
  PageChange(e) {}
  onPressDeleteIcon(plan, index) {
    this.planId = plan;
    this.index = index;
    console.log(this.planId , index);

  }

  // handelDeleteUser() {
  //   this.planProvider.deletedTerritory(this.RootId).subscribe((Res: any) => {
  //     this.data.splice(this.indexOfRootId, 1);
  //   }, err => {
  //     console.log(err);
  //     if (err.error.text === "Entity deleted successfully") {
  //       this.data.splice(this.indexOfRootId, 1);
  //       this.Responsemessage = "Deleted Successfully";
  //       this.error = true;
  //       setTimeout(() => this.error = false, 2000);
  //     } else if(err.error.status == 404) {
  //       this.Responsemessage = "this Felid is maping for Other Felid";
  //       this.error = true;
  //       setTimeout(() => this.error = false, 2000);
  //     } else {
  //       this.Responsemessage = err.error.message;
  //       this.error = true;
  //       setTimeout(() => this.error = false, 2000);
  //     }
  //   });
  // }
  onPressEditIcon(id) {
    this.router.navigate(["/pages/plan/edit-plan", id, this.currentDate]);
  }
  onMapClick(){
    this.router.navigate(["/pages/plan/plan-map"])
  }
}
