"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var AllPlansComponent = /** @class */ (function () {
    function AllPlansComponent(planProvider, router) {
        this.planProvider = planProvider;
        this.router = router;
        this.data = [];
        this.arrayOfTerritoryCodes = [];
    }
    AllPlansComponent.prototype.ngOnInit = function () {
        var _this = this;
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = String(today.getMonth() + 1).padStart(2, "0");
        var yyyy = today.getFullYear();
        this.today = yyyy + "-" + mm + "-" + dd;
        this.planProvider.getCustomersVistsByDate(this.today).subscribe(function (Res) {
            _this.data = Res;
            _this.data = _this.data.reduce(function (unique, o) {
                if (!unique.some(function (obj) { return obj.id.territoryCode === o.id.territoryCode; })) {
                    unique.push(o);
                }
                return unique;
            }, []);
            _this.arrayOfTerritoryCodes = _this.data.map(function (v) { return v.id.territoryCode; });
            _this.getTerritoryName();
        });
    };
    AllPlansComponent.prototype.handleChangeDate = function (date) {
        var _this = this;
        this.currentDate = "";
        this.currentDate = date;
        this.planProvider.getCustomersVistsByDate(date).subscribe(function (Res) {
            _this.data = Res;
            _this.data = _this.data.reduce(function (unique, o) {
                if (!unique.some(function (obj) { return obj.id.territoryCode === o.id.territoryCode; })) {
                    unique.push(o);
                }
                return unique;
            }, []);
            _this.arrayOfTerritoryCodes = _this.data.map(function (v) { return v.id.territoryCode; });
            _this.getTerritoryName();
        });
    };
    AllPlansComponent.prototype.getTerritoryName = function () {
        var _this = this;
        this.planProvider.getArrayOfTerritoryName(this.arrayOfTerritoryCodes).subscribe(function (Res) {
            _this.data.forEach(function (v) { return Res.forEach(function (element) {
                if (v.id.territoryCode == element.territory.territoryCode) {
                    v.territoryName = element.territory.name1;
                }
            }); });
        });
    };
    AllPlansComponent.prototype.PageChange = function (e) { };
    AllPlansComponent.prototype.onPressDeleteIcon = function () { };
    AllPlansComponent.prototype.onPressEditIcon = function (id) {
        this.router.navigate(["/pages/plan/edit-plan", id, this.currentDate]);
    };
    AllPlansComponent.prototype.onMapClick = function () {
        this.router.navigate(["/pages/plan/plan-map"]);
    };
    AllPlansComponent = __decorate([
        core_1.Component({
            selector: "app-all-plans",
            templateUrl: "./all-plans.component.html",
            styleUrls: ["./all-plans.component.sass"]
        })
    ], AllPlansComponent);
    return AllPlansComponent;
}());
exports.AllPlansComponent = AllPlansComponent;
