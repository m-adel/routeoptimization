import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AddPlanComponent } from "./add-plan/add-plan.component";
import { EditPlanComponent } from "./edit-plan/edit-plan.component";
import { AllPlansComponent } from "./all-plans/all-plans.component";
import { PlanComponent } from "./plan.component";
import { PlanRoutingModule } from "./plan.routing";
import { SharedModule } from "src/app/shared/shared.module";
import { FormsModule } from "@angular/forms";
import { CalenderComponent } from "./component/calender/calender.component";
import { AddPanWithMapComponent } from "./add-pan-with-map/add-pan-with-map.component";
import { PlanMapComponent } from "./component/plan-map/plan-map.component";
import { AgmCoreModule } from "@agm/core";
import { NewAngularCalenderComponent } from './component/new-angular-calender/new-angular-calender.component';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { SearchPipe } from './search.pipe';
import { AddNewPlanComponent } from './add-new-plan/add-new-plan.component';
@NgModule({
  declarations: [
    AddPlanComponent,
    EditPlanComponent,
    AllPlansComponent,
    PlanComponent,
    CalenderComponent,
    AddPanWithMapComponent,
    PlanMapComponent,
    NewAngularCalenderComponent,
    SearchPipe,
    AddNewPlanComponent
  ],
  imports: [
    CommonModule,
    PlanRoutingModule,
    SharedModule,
    FormsModule,
    AgmCoreModule,

    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })

  ]
})
export class PlanModule {}
