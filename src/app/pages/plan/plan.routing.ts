import { AllPlansComponent } from './all-plans/all-plans.component';
import { EditPlanComponent } from './edit-plan/edit-plan.component';
import { AddPlanComponent } from './add-plan/add-plan.component';
import {PlanMapComponent} from './component/plan-map/plan-map.component'



import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanComponent } from "./plan.component";
import { AddPanWithMapComponent } from './add-pan-with-map/add-pan-with-map.component';
import { from } from 'rxjs';
import { AddNewPlanComponent } from './add-new-plan/add-new-plan.component';

const routes: Routes = [
    {
        path: '', component: PlanComponent, children: [
            {
                path: 'add-plan', component: AddPlanComponent
            },
            {
                path: "edit-plan/:id/:date", component: EditPlanComponent
            },
            {
                path: "add-new-plan", component: AddNewPlanComponent
            },
            {
                path: "all-plans", component: AllPlansComponent
            },
            {
              path: "add-plan-with-map", component: AddPanWithMapComponent

            },
            {
                path: "plan-map", component: PlanMapComponent
  
            },
            {
                path: '',
                redirectTo: '/pages/plan/all-plans',
                pathMatch: 'full'
            }
        ]
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PlanRoutingModule { }
