import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(customers:any[], term:string): any {
    if(term == undefined){
      return customers
    }

    return customers.filter(function(customers){
      return customers.customerName.toLowerCase().includes(term.toLowerCase())
     });
  }

}


