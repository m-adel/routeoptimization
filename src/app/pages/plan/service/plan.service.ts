import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { WebServicesService } from "src/app/providers/web-services.service";
import { map } from 'rxjs/internal/operators/map';
import { of, Observable, observable } from 'rxjs';
import {forkJoin} from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class PlanService {
  constructor(public http: HttpClient, public config: WebServicesService) {}
  getCustomersVistsByDate(visitDate) {
    const url =
      this.config.serviceUrl +
      this.config.getCustomerVistisByDate
        .replace("{0}", sessionStorage.getItem("companyCode"))
        .replace("{1}", visitDate)
        .replace("{2}", "99999");
    const header = this.config.headers;
    return this.http.get(url, { headers: header });
  }


  getCustomerTerritory(trCode){
    const url =
      this.config.serviceUrl +
      this.config.currentCustomerTerritory
      .replace("{0}", sessionStorage.getItem("companyCode"))
      .replace("{1}", trCode)


    const header = this.config.headers;
    return this.http.get(url, { headers: header });
  }

  getCurrentCustomersVistsByDate(visitDate, code) {
    const url =
      this.config.serviceUrl +
      this.config.getCurrentCustomerVistisByDate
        .replace("{0}", sessionStorage.getItem("companyCode"))
        .replace("{1}", visitDate)
        .replace("{2}", "99999")
        .replace("{3}", code );
    const header = this.config.headers;
    return this.http.get(url, { headers: header });
  }
  searchByName(term, from) {
    if (from === "searchTerritory") {
      let arr = [];
      const url = this.config.serviceUrl + this.config.searchTerritory.
      replace("{0}", term).replace("{1}", sessionStorage.getItem("companyCode"));
      const header = this.config.headers;
      if (term === '') {
        return of([]);
      }
      return this.http.get(url, {headers: header}).pipe( map((response: any) => {
           arr = response.map(element => {
             return element.name1;
           });
           return arr;
          })
        );
    } else {
      let arr = [];
      const url = this.config.serviceUrl + this.config.searchByTaskName.
      replace("{0}", term).replace("{1}", sessionStorage.getItem("companyCode")).replace("{2}", "0");
      const header = this.config.headers;
      if (term === '') {
        return of([]);
      }
      return this.http.get(url, {headers: header}).pipe(map((response: any) => {
           arr = response.map(element => {
             return element.name;
           });
           return arr;
          })
        );
    }
  }
  seachCustomer(code) {
    const url = this.config.serviceUrl + this.config.currentCustomerTerritory
    .replace("{0}", sessionStorage.getItem("companyCode"))
    .replace("{1}", code);
    const header = this.config.headers;
    return this.http.get(url, {headers: header});

  }
  getCurrentTerritory(name, from) {
    if (from === "searchTerritory") {
      const url = this.config.serviceUrl + this.config.searchTerritory.
      replace("{0}", name).replace("{1}", sessionStorage.getItem("companyCode"));
      const header = this.config.headers;
      return this.http.get(url, {headers: header});
    } else {
      const url = this.config.serviceUrl + this.config.searchByTaskName.
      replace("{0}", name).replace("{1}", sessionStorage.getItem("companyCode")).replace("{2}", "0");
      const header = this.config.headers;
      return this.http.get(url, {headers: header});
    }
  }
  getAllCustomerForSingleTerritory(code) {
    const url = this.config.serviceUrl + this.config.GetAllCustomerForTerritory.
    replace("{0}", sessionStorage.getItem("companyCode")).
    replace("{1}", code);
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  assignVistCustomer(data) {
    const url = this.config.serviceUrl + this.config.assignCustomerVisit;
    const header = this.config.headers;
    return this.http.put(url, data, {headers: header});
  }

  assignArrayOfVistCustomer(data) {
    const array = []
    data.forEach(v => {
      const url = this.config.serviceUrl + this.config.assignCustomerVisit;
      const header = this.config.headers;
      array.push(this.http.put(url, v, {headers: header}));
    });
    return Observable.forkJoin(array);
  }
  assignCustomerTask(data) {
    const url = this.config.serviceUrl + this.config.assignTaskCustomer;
    const header = this.config.headers;
    return this.http.post(url, data, {headers: header});
  }
  getCurrentTerritoryForEdit(id) {
    const url = this.config.serviceUrl + this.config.getCuurentTerritory.
    replace("{0}", id).replace("{1}", sessionStorage.getItem("companyCode"));
    const header = this.config.headers;
    return this.http.get(url, {headers: header});
  }
  getArrayOfTerritoryName(ids) {
    const array = [];
    ids.forEach((v, i) => {
      const url = this.config.serviceUrl + this.config.getCuurentTerritory.
      replace("{0}", v).replace("{1}", sessionStorage.getItem("companyCode"));
      const header = this.config.headers;
      array.push(this.http.get(url, {headers: header}));
    });
    return Observable.forkJoin(array);
  }
}
// deletedTerritory(code) {
//   const url = this.config.serviceUrl + this.config.deleteRoot.replace("{territoryCode}", code);
//   const header = this.config.headers;
//   return this.http.delete(url, {headers: header});
// }
