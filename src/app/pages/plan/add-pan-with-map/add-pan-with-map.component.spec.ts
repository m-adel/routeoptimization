import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPanWithMapComponent } from './add-pan-with-map.component';

describe('AddPanWithMapComponent', () => {
  let component: AddPanWithMapComponent;
  let fixture: ComponentFixture<AddPanWithMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPanWithMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPanWithMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
