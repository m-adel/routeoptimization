import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-plan-map',
  templateUrl: './plan-map.component.html',
  styleUrls: ['./plan-map.component.sass']
})
export class PlanMapComponent implements OnChanges {
@Input() latLngArray: any;
@Input() territoryLatLng: any;
@Output() selectedCustomer = new EventEmitter();
private map: google.maps.Map = null;
private heatmap: google.maps.visualization.HeatmapLayer = null;
latitude = 26.8206;
longitude = 30.8025;
zoom = 9;
dataCustomer: any;
  constructor() { }


  ngOnChanges() {
    if (this.territoryLatLng.length > 0) {
      this.latitude = this.territoryLatLng.lat;
      this.longitude = this.territoryLatLng.lng;
    }
  }
  calculateDistance() {
    const mexicoCity = new google.maps.LatLng(this.territoryLatLng.lat, this.territoryLatLng.lng);
    const jacksonville = new google.maps.LatLng(this.latLngArray[0].latitude, this.latLngArray[0].longitude);
    const distance = google.maps.geometry.spherical.computeDistanceBetween(this.territoryLatLng, this.latLngArray[0]);
  }
  onSelectMarker(obj) {
    let flagForSend = true;
    this.latLngArray.forEach((v) => {
      if (v.labal && obj.customerCode == v.customerCode) {
        flagForSend = false;
      }
    });
    if (flagForSend) {
      this.selectedCustomer.emit(obj);
    }
  }
  onClickInfoView() {

  }
  onMouseOver(infoWindow, $event: MouseEvent) {
    infoWindow.open();
}

onMouseOut(infoWindow, $event: MouseEvent) {
    infoWindow.close();
}

}
