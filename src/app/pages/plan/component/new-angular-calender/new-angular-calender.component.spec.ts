import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAngularCalenderComponent } from './new-angular-calender.component';

describe('NewAngularCalenderComponent', () => {
  let component: NewAngularCalenderComponent;
  let fixture: ComponentFixture<NewAngularCalenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewAngularCalenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAngularCalenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
