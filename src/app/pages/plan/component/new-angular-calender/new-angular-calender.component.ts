import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarMonthViewDay } from 'angular-calendar';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours, subHours } from 'date-fns';
import { DayViewHour } from 'calendar-utils';

import { Subject } from 'rxjs';
import * as moment from 'moment';
import * as _ from 'lodash';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-angular-calender',
  templateUrl: './new-angular-calender.component.html',
  styleUrls: ['./new-angular-calender.component.scss']
})
export class NewAngularCalenderComponent implements OnInit, OnChanges {
  viewDate = new Date();
  view = 'month';
  viewD = new Date();
  public events: CalendarEvent[] = [];
  refresh: Subject<any> = new Subject();
  startDay: any = 8;
  endDay: any ;
  endDayProp: any = 25;
  numOfSegement: any = 4;
  hour: Date;
  day: any = false;
  dayView: DayViewHour[];
  @Input() currentEventValue: any;
  @Output() spliceCustomer = new EventEmitter();
  @Output() sendDate = new EventEmitter();
  @Output() selectedTime = new EventEmitter();
  @Input() customerTime: any;
  @Input() resetAllValues: any;
  @Input() currentDayForEdit: any;
  @Input() dateArray: any[];
  @Output() saveArrayOfDates = new EventEmitter();
  eventTemplate: any;
  dateWithChunkes = [];
  flagValue = false;
  activeDayIsOpen = false;
  constructor(public router: Router) {
   }

  ngOnInit() {
    const dateSending =  moment(this.viewDate).format("YYYY-MM-DD");
    this.sendDate.emit(dateSending);
  }

  ngOnChanges(e) {

    if (this.customerTime && (this.customerTime.customerCode || this.customerTime.id.customerCode)   &&  !this.resetAllValues) {
      this.addEvent(this.viewDate);
    }
    if (this.resetAllValues) {
      this.events = [];
    }
    if (this.currentEventValue && this.currentEventValue.length > 0 && this.currentDayForEdit && !this.flagValue) {
        this.viewDate = new Date(this.currentDayForEdit);
        this.viewD = new Date(this.currentDayForEdit);
        const dateSending =  moment(this.viewDate).format("YYYY-MM-DD");
        this.sendDate.emit(dateSending);
        this.showEventsInCalender();
        this.flagValue = true;
    }


  }

  calenderClick() {
    console.log("events" + JSON.stringify(this.events));
  }
  navigateRow() {
  this.hour = null;
  this.viewDate = this.viewD;
  const dateSending =  moment(this.viewDate).format("YYYY-MM-DD");
  this.sendDate.emit(dateSending);
}
  monthView() {
    this.view = 'month';
    this.hour = null;
  }
  dayClicked(e) {
    const dateCheck = this.viewDate.getDay() ==  e.day.date.getDay();
    const dateCheckPast = e.day.isFuture  || e.day.isToday;
    const checkDay = moment(this.viewDate).format("YYYY-MM-DD") == moment(e.day.date).format("YYYY-MM-DD");
    if (this.router.url.includes("/pages/plan/edit-plan") ) {
        if (checkDay) {
          this.view = 'day';
          this.day = true;
          this.viewDate = e.day.date;
          this.viewD = e.day.date;
          const dateSending =  moment(this.viewDate).format("YYYY-MM-DD");
          this.sendDate.emit(dateSending);
        }

    } else if ( (this.dateArray && this.dateArray.length > 0 ) || ( dateCheckPast  )) {
      if ( this.dateArray &&  this.dateArray.length > 0 && dateCheck ) {
        this.view = 'day';
        this.day = true;
        this.viewDate = e.day.date;
        this.viewD = e.day.date;
        const dateSending =  moment(this.viewDate).format("YYYY-MM-DD");
        this.sendDate.emit(dateSending);
      } else if ((!this.dateArray ||  this.dateArray.length == 0) && !dateCheck  ) {
        this.view = 'day';
        this.day = true;
        this.viewDate = e.day.date;
        this.viewD = e.day.date;
        const dateSending =  moment(this.viewDate).format("YYYY-MM-DD");
        this.events = [];
        this.sendDate.emit(dateSending);
      } else if ( this.dateArray &&  this.dateArray.length > 0 && dateCheckPast) {
        this.view = 'day';
        this.day = true;
        this.viewDate = e.day.date;
        this.viewD = e.day.date;
        const dateSending =  moment(this.viewDate).format("YYYY-MM-DD");
        this.sendDate.emit(dateSending);
      } else if (dateCheckPast && dateCheck) {
        this.view = 'day';
        this.day = true;
        this.viewDate = e.day.date;
        this.viewD = e.day.date;
        const dateSending =  moment(this.viewDate).format("YYYY-MM-DD");
        this.sendDate.emit(dateSending);
      }
   } else if ( (dateCheckPast && dateCheck)) {
      this.view = 'day';
      this.day = true;
      this.viewDate = e.day.date;
      this.viewD = e.day.date;
      const dateSending =  moment(this.viewDate).format("YYYY-MM-DD");
      this.sendDate.emit(dateSending);
   }
  }
  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    body.forEach(day => {
        if (!this.dateIsValid(day.date)) {
          day.cssClass = 'disabled';
        }
    });
  }
  dateIsValid(date: Date): boolean {
    const  minDate: Date = subDays(new Date(), 1);
    return date >= minDate;
}
  changeTime(e) {
    const orignalStart = JSON.parse(JSON.stringify(e.event.start));
    const orignalEnd = JSON.parse(JSON.stringify(e.event.end));
    const valueOfStart = new Date(orignalStart);
    const valueOfEnd = new Date(orignalEnd);
    console.log(e);
    if (e.type == "resize") {
     if (this.dateArray && this.dateArray.length > 0) {
       this.events.forEach((v, i) => {
      if (moment(v.start).format("HH:mm") == moment(e.event.start).format("HH:mm") ||
      moment(v.end).format("HH:mm") == moment(e.event.end).format("HH:mm")) {
        const sTime = moment(e.newStart).format("HH:mm");
        const eTime = moment(e.newEnd).format("HH:mm");
        const day = moment(v.start).format("YYYY-MM-DD");
        const startTime = new Date(day + "T" + sTime);
        const endTime = new Date(day + "T" + eTime);
        v.start = startTime;
        v.end = endTime;
      }
       });
     } else {
      this.events.forEach((v, i) => {
        if (v.start.getTime() == e.event.start.getTime() && v.end.getTime() == e.event.end.getTime()) {
          v.start = e.newStart;
          v.end = e.newEnd;
        }
      });
     }
     this.events.forEach( v => this.shifting(v));

    } else {
      this.events.forEach((v, i) => {
        if (v.start.getTime() == e.event.start.getTime() && v.end.getTime() == e.event.end.getTime()) {
          v.start = e.newStart;
          v.end = e.newEnd;
          this.events.forEach((ele, i) => {
            if (v.end.getTime() > ele.start.getTime() && v.start.getTime() < ele.end.getTime() && v.title != ele.title) {
              v.start =  ele.start;
              v.end = ele.end;
              ele.start = valueOfStart;
              ele.end = valueOfEnd;
            }
          });
        }
      });
    }
    if (this.dateArray && this.dateArray.length > 0) {
      this.saveArrayOfDates.emit(this.events);
    } else {

      this.selectedTime.emit(this.events);
    }
    this.refresh.next();
  }
  beforeDayViewRender(dayView: DayViewHour[], viewdate) {
    this.dayView = dayView;
  }
  selectHour(e) {
    this.hour = e.date;
  }
  addEvent(viewDate: Date) {
    if (this.dateArray && this.dateArray.length > 0) {
      this.AddEventsInCalenderForArray();
    } else {
      const selectedDate = moment(viewDate).format("YYYY-MM-DD");
      const starting = new Date ( selectedDate + "T08:00:00.000Z");
      starting.setTime( starting.getTime() + starting.getTimezoneOffset() * 60 * 1000 );
      const ending = new Date (selectedDate + "T08:15:00.000Z");
      ending.setTime( ending.getTime() + ending.getTimezoneOffset() * 60 * 1000 );
      if (this.events.length == 0) {
        this.events.push({start: starting, end: ending,
          title: this.customerTime.customerName, draggable: true, meta: this.customerTime,
            resizable: {beforeStart: false, afterEnd: true}});
      } else {
          const lastHourSelected = this.events[this.events.length - 1];
          const nextHour =  moment (lastHourSelected.end, "HH:mm") ;
          const nextEndHour = nextHour.add(15, 'minutes').format("HH:mm");
          const nextHourr = moment (lastHourSelected.end).format("HH:mm") ;
          const newStart = new Date(selectedDate + `T${nextHourr}:00.000Z`);
          newStart.setTime( newStart.getTime() + newStart.getTimezoneOffset() * 60 * 1000 );
          const newEnd = new Date(selectedDate + `T${nextEndHour}:00.000Z`);
          newEnd.setTime( newEnd.getTime() + newEnd.getTimezoneOffset() * 60 * 1000 );
          this.events.push({start: newStart, end: newEnd,
            title: this.customerTime.customerName, draggable: true, meta: this.customerTime,
            resizable: {beforeStart: true, afterEnd: true}});
      }
      this.refresh.next();
      this.selectedTime.emit(this.events);
    }


  }
  SpliceEvent(e) {
    console.log(e);
    const i = this.events.findIndex(x => x.title == e.event.title);
    this.events.splice(i , 1);
    if (this.dateArray && this.dateArray.length > 0) {
      this.saveArrayOfDates.emit(this.events);
    } else {
      this.spliceCustomer.emit({object: e, index: i});
    }
    this.refresh.next();
  }
  shifting(v) {
    this.events.forEach(ele => {
      if (v.end.getTime() > ele.start.getTime() && v.start.getTime() < ele.end.getTime() && v.title != ele.title) {
        ele.start = v.end;
      }
      });
  }
  showEventsInCalender() {
    this.currentEventValue.forEach((v, i) => {
      const selectedDate = moment(this.viewDate).format("YYYY-MM-DD");
      const starting = new Date(selectedDate + `T${v.startTime }:00.000Z`);
      starting.setTime( starting.getTime() + starting.getTimezoneOffset() * 60 * 1000 );
      const ending = new Date(selectedDate + `T${v.endTime }:00.000Z`);
      ending.setTime( ending.getTime() + ending.getTimezoneOffset() * 60 * 1000 );
      this.events.push({start: starting, end: ending,
        title: v.customerName, draggable: true,
         meta: {customerName: v.customerName ,
           companyCode: v.id.companyCode,
           customerCode: v.id.customerCode,
           territoryCode: v.id.territoryCode,
           visitDate: v.id.visitDate
          },
        resizable: {beforeStart: true, afterEnd: true}});
    });
    this.selectedTime.emit(this.events);

  }
  AddEventsInCalenderForArray() {
    if (this.dateWithChunkes.length == 0) {
      this.dateWithChunkes = _.chunk(this.dateArray, 1);
    }
    const num = this.events.length;
    this.dateArray.forEach((v, i) => {
      const selectedDate = moment(v).format("YYYY-MM-DD");
      const starting = new Date(selectedDate + "T08:00:00.000Z");
      const ending = new Date(selectedDate + "T08:15:00.000Z");
      starting.setTime( starting.getTime() + starting.getTimezoneOffset() * 60 * 1000 );
      ending.setTime( ending.getTime() + ending.getTimezoneOffset() * 60 * 1000 );
      console.log(starting);
      console.log(ending);




      if (this.events.length < this.dateArray.length) {
        this.events.push({start: starting, end: ending,
          title: this.customerTime.customerName, draggable: true, meta: this.customerTime,
            resizable: {beforeStart: false, afterEnd: true}});
      } else {
          const indexx =  num - (this.dateArray.length - 1);
          const lastHourSelected = this.events[indexx] || this.events[indexx + 1]  ;
          const nextHour =  moment (lastHourSelected.end, "HH:mm") ;
          const nextEndHour = nextHour.add(15, 'minutes').format("HH:mm");
          const nextHourr = moment (lastHourSelected.end).format("HH:mm") ;
          const newStart = new Date(selectedDate + `T${nextHourr}:00.000Z`);
          newStart.setTime( newStart.getTime() + newStart.getTimezoneOffset() * 60 * 1000 );
          const newEnd = new Date(selectedDate + `T${nextEndHour}:00.000Z`);
          newEnd.setTime( newEnd.getTime() + newEnd.getTimezoneOffset() * 60 * 1000 );
          this.events.push({start: newStart, end: newEnd,
            title: this.customerTime.customerName, draggable: true, meta: this.customerTime,
            resizable: {beforeStart: true, afterEnd: true}});
          }
        });
    this.saveArrayOfDates.emit(this.events);
    this.refresh.next();
  }
}
