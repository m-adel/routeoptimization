import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-calender",
  templateUrl: "./calender.component.html",
  styleUrls: ["./calender.component.sass"]
})
export class CalenderComponent implements OnInit, OnChanges {
  @Output() sendDate = new EventEmitter();
  @Output() selectedTime = new EventEmitter();
  @Output() spliceCustomer = new EventEmitter();
  @Input() customerTime: any;
  @Input() resetAllValues: any;
  @Input() currentDayForEdit: any;
  date;
  days = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
  mounthDays = [];
  startDay;
  DayArray = [];
  showMonth;
  month;
  year;
  counter = 0;
  counterPrev = 0;
  today;
  schedule = true;
  dragableSwepper: any;
  hours = [
    {houre: "00:00", data: { customerName: ""}},
    {houre: "01:00", data: { customerName: ""}},
    {houre: "02:00", data: { customerName: ""}},
    {houre: "03:00", data: { customerName: ""}},
    {houre: "04:00", data: { customerName: ""}},
    {houre: "05:00", data: { customerName: ""}},
    {houre: "06:00", data: { customerName: ""}},
    {houre: "07:00", data: { customerName: ""}},
    {houre: "08:00", data: { customerName: ""}},
    {houre: "09:00", data: { customerName: ""}},
    {houre: "10:00", data: { customerName: ""}},
    {houre: "11:00", data: { customerName: ""}},
    {houre: "12:00", data: { customerName: ""}},
    {houre: "13:00", data: { customerName: ""}},
    {houre: "14:00", data: { customerName: ""}},
    {houre: "15:00", data: { customerName: ""}},
    {houre: "16:00", data: { customerName: ""}},
    {houre: "17:00", data: { customerName: ""}},
    {houre: "18:00", data: { customerName: ""}},
    {houre: "19:00", data: { customerName: ""}},
    {houre: "20:00", data: { customerName: ""}},
    {houre: "21:00", data: { customerName: ""}},
    {houre: "22:00", data: { customerName: ""}},
    {houre: "23:00", data: { customerName: ""}},

  ];
  constructor(private router: Router) {

  }
  ngOnInit() {
    const now = new Date();
    const day = now.getDate();
    this.month = now.getMonth();
    this.year = now.getFullYear();
    this.daysInMonth(this.month, this.year);
    this.counter = this.month;
    this.getDayToday()
  }
  ngOnChanges(e) {
    console.log(e);
    if (this.resetAllValues) {
       this.hours.map(v => v.data = {customerName: ""});
    }
    let flag = true;
    if (this.customerTime && !this.customerTime.startTime) {
      this.hours.forEach((v, i) => {
        if (!v.data.customerName && flag == true) {
          v.data = this.customerTime;
          flag = false;
          this.schedule = false;
        }
      });
      this.selectedTime.emit(this.hours);
    } else if (e.customerTime && e.customerTime.currentValue && e.customerTime.currentValue.startTime  ) {
      this.hours.forEach((v, i) => {
        if (v.houre == this.customerTime.startTime) {
          v.data =  this.customerTime;
          this.schedule = false;
        }
      });
    }


  }
  getDayToday() {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, "0");
    const mm = String(today.getMonth() + 1).padStart(2, "0");
    const yyyy = today.getFullYear();
    this.today = dd;
    const day = `${yyyy}-${mm}-${dd}`;
    this.sendDate.emit(day);
    if (this.currentDayForEdit) {
      setTimeout(() => {
        this.today = this.currentDayForEdit.split("-")[2];
        this.sendDate.emit(this.currentDayForEdit);
      }, 200);
    }
  }

  daysInMonth(month, year) {
    const days = this.getMonthDays(month + 1, year);
    this.showMonth = new Date(year, month, 1);
    const startDay = this.showMonth.getDay();

    this.setThePrintLoop(startDay, days);
  }

  setThePrintLoop(startDay, days) {
    for (let i = 0; i < 42; i++) {
      if (i >= startDay && i - startDay < days) {
        this.DayArray.push(i - startDay + 1);
      } else {
        this.DayArray.push("");
      }
    }
    console.log(this.DayArray, "Values ");
  }

  getMonthDays(month, year) {
    let days;
    if (
      month === 1 ||
      month === 3 ||
      month === 5 ||
      month === 7 ||
      month === 8 ||
      month === 10 ||
      month === 12
    ) {
      days = 31;
    } else if (month === 4 || month === 6 || month === 9 || month === 11) {
      days = 30;
    } else if (month === 2) {
      if (this.isLeapYear(year)) {
        days = 29;
      } else {
        days = 28;
      }
    }
    return days;
  }

  isLeapYear(year) {
    if (year % 4 === 0) {
      return true;
    } else {
      return false;
    }
  }
  onPressDay(i) {
    if (this.router.url.includes("edit-plan")) {

    } else {
      const day = i.toString();
      const month = (this.month + 1).toString().padStart(2, "0");
      const year = this.year.toString().padStart(2, "0");
      const date = year + "-" + month + "-" + day;
      this.today = day;
      this.schedule = this.router.url === "/pages/plan/add-plan-with-map" ? false : true;
      this.sendDate.emit(date);
    }
    // this.router.navigate(["/Pages/doctor/add-exchange-doctor", this.urlSource, date]);
  }

  OnPressNext() {
    this.counterPrev = 0;
    this.DayArray = [];
    if (this.month < 11 && this.month !== 0) {
      this.counter = this.counter + 1;
      this.month = this.counter;
      const days = this.getMonthDays(this.month + 1, this.year);
      this.showMonth = new Date(this.year, this.month, 1);
      const startDay = this.showMonth.getDay();
      this.setThePrintLoop(startDay, days);
    } else if (this.month === 11) {
      this.counter = 0;
      this.month = 0;
      this.month = this.month + this.counter;
      this.year = this.year + 1;
      const days = this.getMonthDays(this.month + 1, this.year);
      this.showMonth = new Date(this.year, this.month, 1);
      const startDay = this.showMonth.getDay();
      this.setThePrintLoop(startDay, days);
    } else {
      this.counter = this.counter + 1;
      this.month = this.counter + this.month;
      const days = this.getMonthDays(this.month + 1, this.year);
      this.showMonth = new Date(this.year, this.month, 1);
      const startDay = this.showMonth.getDay();
      this.setThePrintLoop(startDay, days);
    }
  }
  OnPressPrev() {
    this.DayArray = [];
    if (this.month < 11 && this.month !== 0) {
      this.month = this.month - 1;
      this.counter = this.month;
      const days = this.getMonthDays(this.month + 1, this.year);
      this.showMonth = new Date(this.year, this.month, 1);
      const startDay = this.showMonth.getDay();
      this.setThePrintLoop(startDay, days);
    } else {
      if (this.counterPrev === 0 && this.month === 0) {
        this.month = 11;
        this.counter = this.month;
        this.year = this.year - 1;
        const days = this.getMonthDays(this.month + 1, this.year);
        this.showMonth = new Date(this.year, this.month, 1);
        const startDay = this.showMonth.getDay();
        this.setThePrintLoop(startDay, days);
        this.counterPrev = 1;
      } else {
        this.month = this.month - 1;
        this.counter = this.month;
        const days = this.getMonthDays(this.month + 1, this.year);
        this.showMonth = new Date(this.year, this.month, 1);
        const startDay = this.showMonth.getDay();
        this.setThePrintLoop(startDay, days);
        this.counterPrev = 0;
      }
    }
  }
  onPressScheduleIcon() {
    this.schedule = true;
  }
  onPressSelectTime(h, i) {
    this.hours.forEach((v, ind) => {document.getElementById(ind.toString()).style.backgroundColor = "#fff" ; });
    document.getElementById(i).style.backgroundColor = "#c2c2e6";
  }
  dragStart(event, i) {
    this.dragableSwepper = i;
  }
  allowDrop(event) {
    event.preventDefault();
  }
  drop(event, index) {
    event.preventDefault();
    this.hours.forEach((v, i) => {
      if (!v.data.customerName && i == index) {
        v.data = this.hours[this.dragableSwepper].data;
        this.hours[this.dragableSwepper].data = {customerName: ""};
      } else if (v.data.customerName && i == index) {
        const element = this.hours[index].data;
        this.hours[index].data = this.hours[this.dragableSwepper].data;
        this.hours[this.dragableSwepper].data = element;
      }
    });
    this.selectedTime.emit(this.hours);
  }
  onPressClose(obj, i) {
    this.spliceCustomer.emit({object: obj, index: i});
    this.hours[i].data = {customerName: ""};

  }


}
