export interface TaskCustomers {
  taskCustomers: TaskCustomersArray[];
}
interface TaskCustomersArray {
  [x: string]: any;
  [index: number]: {
    active: boolean;
    customerCode: string;
    territoryCode: string;
    validFrom: string;
    validTo: string;
    taskCompany: {
      id: {
        taskId: number;
        companyCode: string;
      };
    };
    taskCustomerExclusions: TaskCustomerExclusions[];
  };
}
interface TaskCustomerExclusions {
  [index: number]: {
    customerCode: string;
    excludedDate: string;
    territoryCode: string;
  };
}
