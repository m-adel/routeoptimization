export interface TerritoryCustomerVisits {
  companyCode: string;
  territoryCode: string;
  visitDate: string;
  territoryCustomerVisits: TerritoryCustomerVisitsArray[];
}
interface TerritoryCustomerVisitsArray {
  [index: number]: {
    id: {
      companyCode: string;
      territoryCode: string;
      customerCode: string;
      visitDate: string;
    };
    endTime: string;
    startTime: string;
    visitOrder: number;
  };
}

export interface ArrayTerritoryCustomerVisits {
  [index: number]: {
    companyCode: string;
    territoryCode: string;
    visitDate: string;
    territoryCustomerVisits: TerritoryCustomerVisitsArray[];
  };
}
