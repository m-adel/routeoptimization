import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  map,
  tap,
  switchMap
} from "rxjs/operators";
import { Router } from "@angular/router";
import { PlanService } from "../service/plan.service";
import { TaskCustomers } from "../interface/task-customers";
import {
  TerritoryCustomerVisits,
  ArrayTerritoryCustomerVisits
} from "../interface/territory-customer-visits";
import * as moment from "moment";
import * as _ from 'lodash';
@Component({
  selector: "app-add-plan",
  templateUrl: "./add-plan.component.html",
  styleUrls: ["./add-plan.component.sass"]
})
export class AddPlanComponent implements OnInit {
  searching = false;
  searchFailed = false;
  searchFrom = "";
  taskCustomer: TaskCustomers = {} as TaskCustomers;
  arrayTerritoryCustomerVisits = [];
  territoryCustomerVisit: TerritoryCustomerVisits = {
    territoryCustomerVisits: []
  } as TerritoryCustomerVisits;
  territoryCode: any;
  arrayOfCustomers = [];
  arrayOfCustomersName = [];
  arrayOfCustomerSelected = [];
  clearValue = false;
  dragableSwepper: any;
  addTaskBtn = false;
  arrayOfTasks = [];
  dateJourny: any;
  success = false;
  error = false;
  Responsemessage = "";
  repeatedValue = "No Repeat";
  from: any;
  to: any;
  dateArray = [];
  customerTime: any;
  resetAllValues = false;
  pressOK = false;
  territoryName: any;
  currentDayDate: any;
  constructor(public router: Router, private planProvider: PlanService) {
    this.currentDayDate = moment().format('YYYY-MM-DD');

  }
  formatter = (result: string) => result.toUpperCase();
  search = (text$: Observable<string>) =>
    text$.pipe( debounceTime(300), distinctUntilChanged(), tap(() => (this.searching = true)), switchMap(term =>
        this.planProvider.searchByName(term, this.searchFrom).pipe(tap(), catchError(() => {
            this.searchFailed = true;
            return [];
          }))), tap(() => (this.searching = false)))

  ngOnInit() {}
  onChange(e, from) {
    if (from == "searchTerritory") {
      this.planProvider.getCurrentTerritory(e.item, from).subscribe(Res => {
        this.territoryCode = "";
        this.territoryCode = Res[0].territoryCode;
        this.territoryName = Res[0].name1;
        this.getAllCustomerOfTerritory();
      });
    }
  }
  SearchFrom(e) {
    this.searchFrom = e === "searchTerritory" ? "searchTerritory" : "searchTask";
  }

  getAllCustomerOfTerritory() {
    this.planProvider.getAllCustomerForSingleTerritory(this.territoryCode).subscribe((Res: any) => {
        this.arrayOfCustomerSelected = [];
        this.arrayOfCustomers = Res;
        this.arrayOfCustomersName = this.arrayOfCustomers.map(v => v.customerName );
        this.resetAllValues = true;
        console.log(this.arrayOfCustomers);
        
      });
  }
  ResiveCustomer(event) {
    const customerName = event.selectedData.toUpperCase();
    this.arrayOfCustomers.forEach((v, i) => {
      if (v.customerName.toUpperCase() == customerName) {
        this.arrayOfCustomerSelected.push(v);
        this.arrayOfCustomers.splice(i, 1);
        this.arrayOfCustomersName.splice(i, 1);
        this.clearValue = true;
        this.resetAllValues = false;
      }
    });
    this.clearValue = false;
    const inde = this.arrayOfCustomerSelected.findIndex( x => x.customerName == event.selectedData);
    this.customerTime = this.arrayOfCustomerSelected[inde];
  }
  handleSpliceCustomerArray(event) {
    const index = this.arrayOfCustomerSelected.findIndex( x => x.customerCode == event.object.event.meta.customerCode);
    let arr = JSON.parse(JSON.stringify(this.territoryCustomerVisit.territoryCustomerVisits));
    const indexOfVisit = arr.findIndex( x => x.id.customerCode == event.object.event.meta.customerCode);
    this.territoryCustomerVisit.territoryCustomerVisits.splice(indexOfVisit, 1);
    this.arrayOfCustomerSelected.splice(index, 1);
    this.arrayOfCustomers.push(event.object.event.meta);
    this.arrayOfCustomersName.push(event.object.event.meta.customerName);
    this.customerTime = null;

  }

  ReciveDate(e) {
    this.dateJourny = e;
  }
  ReciveTime(event) {
    event.sort((a, b) => a.start - b.start);
    this.resetAllValues = false;
    const arr = [];
    let counter = 0;
    this.territoryCustomerVisit.companyCode = sessionStorage.getItem("companyCode" );
    this.territoryCustomerVisit.territoryCode = this.territoryCode;
    this.territoryCustomerVisit.visitDate = this.dateJourny;
    event.forEach((v, i) => {
      if (v.meta.customerName) {
        counter++;
        arr.push({
          id: {
            companyCode: sessionStorage.getItem("companyCode"),
            territoryCode: this.territoryCode,
            customerCode: v.meta.customerCode,
            visitDate: this.dateJourny
          },
          endTime: moment(v.end).format("YYYY-MM-DDTHH:mm:ss"),
          startTime: moment(v.start).format("YYYY-MM-DDTHH:mm:ss"),
          visitOrder: i + 1
        });
      }
    });
    this.territoryCustomerVisit.territoryCustomerVisits = arr;
  }
  handleSubmitAddJournyPlan() {
    this.onSavePlan();
  }
  onSavePlan() {
    if (this.dateArray.length > 0) {
      this.saveArrayOfPlans();
    } else {
      this.planProvider.assignVistCustomer(this.territoryCustomerVisit).subscribe(Res => {
            this.success = true;
            this.error = false;
            this.Responsemessage = "Success To Add Visit";
            setTimeout(() => {
              this.success = false;
              if (this.dateArray.length === 0) {
                this.router.navigateByUrl("/pages/plan/all-plans");
              }
            }, 3000);
          },
          error => {
            this.error = true;
            this.success = false;
            this.Responsemessage = error.error.message;
          }
        );
    }
  }
  onSelectRepeatedValue(e) {
    this.repeatedValue = e.target.value;
    if (this.repeatedValue == "No Repeat") {
      this.dateArray = [];
      this.arrayTerritoryCustomerVisits = [];

    }
  }
  onSelectDate(from, event) {
    if (this.from && this.to) {
      this.calaculateRepeated();
    }
  }
  calaculateRepeated() {
    const datesArr = [];
    let currentDate = moment(this.from);
    const stopDate = moment(this.to);
    if (this.repeatedValue === "Daily") {
      while (currentDate <= stopDate) {
        datesArr.push(moment(currentDate).format("YYYY-MM-DD"));
        currentDate = moment(currentDate).add(1, "days");
      }
    } else if (this.repeatedValue === "Monthly") {
      const dateStart = moment(this.from);
      const dateEnd = moment(this.to);
      while (
        dateEnd > dateStart ||
        dateStart.format("M-D") === dateEnd.format("M-D")
      ) {
        datesArr.push(dateStart.format("YYYY-MM-DD"));
        dateStart.add(1, "month");
      }
    } else if (this.repeatedValue === "Weekly") {
      const start = moment(this.from);
      const end = moment(this.to).add("d");
      const daysWillRepeate = start.day();
      const tmp = start.clone().day(daysWillRepeate);
      if (tmp.isAfter(start, "d")) {
        datesArr.push(tmp.format("YYYY-MM-DD"));
      }
      while (tmp.isBefore(end)) {
        if (datesArr.length == 0) {
          tmp.add(0, "days");
        } else {
          tmp.add(7, "days");
        }
        datesArr.push(tmp.format("YYYY-MM-DD"));
      }
      // datesArr.splice(this.dateArray.length - 1, 1);
    }
    this.dateArray = datesArr;
    // this.saveArrayOfDates();
  }
  saveArrayOfDates(e) {
    this.arrayTerritoryCustomerVisits = [];
    this.dateArray.forEach(element => {
      this.arrayTerritoryCustomerVisits.push({
        companyCode: sessionStorage.getItem("companyCode"),
        territoryCode: this.territoryCode,
        visitDate: element,
        territoryCustomerVisits: [],
      });
    });

    this.arrayTerritoryCustomerVisits.forEach((elem, inde) => {
      e.forEach((v, i) => {
        this.arrayTerritoryCustomerVisits[inde].territoryCustomerVisits.push({
          id: {
            companyCode: sessionStorage.getItem("companyCode"),
            territoryCode: this.territoryCode,
            customerCode: v.meta.customerCode,
            visitDate: elem.visitDate,
          },
          visitOrder: 0,
          endTime: moment(v.end).format("YYYY-MM-DD") == elem.visitDate ? moment(v.end).format("YYYY-MM-DDTHH:mm:ss") : null,
          startTime:  moment(v.start).format("YYYY-MM-DD") == elem.visitDate ? moment(v.start).format("YYYY-MM-DDTHH:mm:ss") : null
        });
      });
    });
    this.arrayTerritoryCustomerVisits.forEach(v => {
      v.territoryCustomerVisits = v.territoryCustomerVisits.filter(obj => obj.startTime != null);
      v.territoryCustomerVisits.sort((a, b) => a.start - b.start);
      v.territoryCustomerVisits.forEach((element, index) => element.visitOrder = index + 1 );
    });
    console.log(this.arrayTerritoryCustomerVisits);
    // this.saveArrayOfPlans();
  }
  saveArrayOfPlans() {
    this.planProvider
      .assignArrayOfVistCustomer(this.arrayTerritoryCustomerVisits)
      .subscribe(
        Res => {
          this.success = true;
          this.Responsemessage = "Succesfully Added !";
          setTimeout(() => {
            this.router.navigateByUrl("/pages/plan/all-plans");
          }, 2000);
        },
        error => {
          this.error = true;
          this.success = false;
          this.Responsemessage = error.error.message;
        }
      );
  }
  onPressCancel() {
    this.router.navigateByUrl("/pages/plan/all-plans");
  }
  onPressOk() {
    if (this.territoryCode && this.repeatedValue == "No Repeat") {
      this.pressOK = true;
      this.error = false;
    } else if (!this.territoryCode  && this.repeatedValue == "No Repeat") {
      this.error = true;
      this.Responsemessage = "Please Enter Your Territory You Want To Add !";
    } else  if (this.territoryCode && this.repeatedValue != "No Repeat" && this.dateArray.length > 0) {
      this.pressOK = true;
      this.error = false;
    } else if (this.territoryCode && this.repeatedValue != "No Repeat" && this.dateArray.length  == 0) {
      this.error = true;
      this.Responsemessage = "Make Sure The To (Date) Greater Than From (Date)";
    }
  }
  onPressToRestValue() {
    this.resetAllValues = true;
    this.arrayTerritoryCustomerVisits = [];
    this.territoryCustomerVisit = {
      companyCode: "",
      territoryCode: "",
      visitDate: "",
      territoryCustomerVisits: []
    };
    this.territoryCode = null;
    this.arrayOfCustomers = [];
    this.arrayOfCustomersName = [];
    this.arrayOfCustomerSelected = [];
    this.success = false;
    this.error = false;
    this.Responsemessage = "";
    this.repeatedValue = "No Repeat";
    this.from = null;
    this.to = null;
    this.dateArray = [];
    this.pressOK = false;
  }
}
