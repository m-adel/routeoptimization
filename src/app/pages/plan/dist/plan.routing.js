"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var all_plans_component_1 = require("./all-plans/all-plans.component");
var edit_plan_component_1 = require("./edit-plan/edit-plan.component");
var add_plan_component_1 = require("./add-plan/add-plan.component");
var plan_map_component_1 = require("./component/plan-map/plan-map.component");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var plan_component_1 = require("./plan.component");
var add_pan_with_map_component_1 = require("./add-pan-with-map/add-pan-with-map.component");
var routes = [
    {
        path: '', component: plan_component_1.PlanComponent, children: [
            {
                path: 'add-plan', component: add_plan_component_1.AddPlanComponent
            },
            {
                path: "edit-plan/:id/:date", component: edit_plan_component_1.EditPlanComponent
            },
            {
                path: "all-plans", component: all_plans_component_1.AllPlansComponent
            },
            {
                path: "add-plan-with-map", component: add_pan_with_map_component_1.AddPanWithMapComponent
            },
            {
                path: "plan-map", component: plan_map_component_1.PlanMapComponent
            },
            {
                path: '',
                redirectTo: '/pages/plan/all-plans',
                pathMatch: 'full'
            }
        ]
    },
];
var PlanRoutingModule = /** @class */ (function () {
    function PlanRoutingModule() {
    }
    PlanRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], PlanRoutingModule);
    return PlanRoutingModule;
}());
exports.PlanRoutingModule = PlanRoutingModule;
