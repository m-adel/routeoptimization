import { Component } from "@angular/core";

@Component({
  selector: "app-Plan",
  template: `
  <!-- <app-child-navs [navs]="navs"></app-child-navs> -->

    <router-outlet></router-outlet>
  `
})
export class PlanComponent {
  navs = [
    { name: "All Plans", link: "/pages/plan/all-plans" },
    { name: "Add Plan", link: "/pages/plan/add-plan" },
    {name: "Map", link: "/pages/plan/add-plan-with-map"},

  ];
}
