import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, filter } from "rxjs/operators";
import { PlanService } from '../service/plan.service';
import * as moment from "moment";
import { TerritoryCustomerVisits } from '../interface/territory-customer-visits';
import { Router } from '@angular/router';
import { CustomerService } from './../../customer/service/customer.service';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Subject, merge } from 'rxjs';
@Component({
  selector: 'app-add-new-plan',
  templateUrl: './add-new-plan.component.html',
  styleUrls: ['./add-new-plan.component.scss']
})
export class AddNewPlanComponent implements OnInit {

  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  searchFailed = false;
  searching = false;
  data = [];
  term
  obj
  pageSize: any = 10;
  offset = 0;
  loaded = true;
  totalItems: any = 0;
  clearValue
  searchFrom = "";
  territoryCode: string;
  arrayOfCustomerSelected = [];
  arrayOfCustomers: any[];
  arrayOfCustomersName: any[];
  territoryLatLng: { lat: any; lng: any; };
  customerTime: any;
  dateJourny: any;
  resetAllValues = false;
  success = false;
  error = false;
  Responsemessage = "";
  pressOK = false;
  dateArray = [];
  repeatedValue = "No Repeat";
  arrayTerritoryCustomerVisits = [];
  from: any;
  to: any;
  territoryName: any;
  currentDayDate: any;
  territoryCustomerVisit: TerritoryCustomerVisits = { territoryCustomerVisits: [] } as TerritoryCustomerVisits;



  
  constructor(public planProvider: PlanService, public router: Router, public customer: CustomerService) {
    this.currentDayDate = moment().format('YYYY-MM-DD');
  }
  formatter = (result: string) => result.toUpperCase();
  // route search
  search = (text$: Observable<string>) =>
    text$.pipe(debounceTime(300), distinctUntilChanged(), tap(() => (this.searching = true)),
      switchMap(term => this.planProvider.searchByName(term, this.searchFrom).pipe(tap(),
        catchError(() => {
          this.searchFailed = true; return [];
        }))), tap(() => (this.searching = false)))

  ngOnInit() {
  }
  
  SearchFrom(e) {
    this.searchFrom = e === "searchTerritory" ? "searchTerritory" : "searchTask";
  }
  onChange(e, from) {
    if (from == "searchTerritory") {
      this.planProvider.getCurrentTerritory(e.item, from).subscribe((Res) => {
        this.resetAllValues = true;
        console.log(Res[0]);
        this.territoryLatLng = { lat: Res[0].latitude, lng: Res[0].longitude };
        this.territoryCode = "";
        this.territoryCode = Res[0].territoryCode;
        this.territoryName = Res[0].name1;
        this.getAllCustomerOfTerritory();
      });
    }
  }
  getAllCustomerOfTerritory() {
    this.planProvider.getAllCustomerForSingleTerritory(this.territoryCode).subscribe((Res: any) => {
      this.arrayOfCustomers = Res;
      this.arrayOfCustomers.forEach(cus => cus.labal = "");
      this.arrayOfCustomerSelected = [];
      this.arrayOfCustomers = Res;
      this.arrayOfCustomersName = this.arrayOfCustomers.map(v => v.customerName);
      this.resetAllValues = true;
      console.log(this.arrayOfCustomersName);
      console.log(this.arrayOfCustomers);


    });
  }
  ResiveCustomer(event) {
    const customerName = event.selectedData.toUpperCase();
    this.arrayOfCustomers.forEach((v, i) => {
      if (v.customerName.toUpperCase() == customerName) {
        this.arrayOfCustomerSelected.push(v);
        this.arrayOfCustomers.splice(i, 1);
        this.arrayOfCustomersName.splice(i, 1);
        this.clearValue = true;
        this.resetAllValues = false;
        console.log(v);

      }
    });
    this.clearValue = false;
    const inde = this.arrayOfCustomerSelected.findIndex(x => x.customerName == event.selectedData);
    this.customerTime = this.arrayOfCustomerSelected[inde];
  }

  // add from customer table
  addToCalendar(index) {
  
    this.data.push(this.arrayOfCustomers[index])
    this.arrayOfCustomers.splice(index, 1);
    this.arrayOfCustomersName.splice(index, 1);
    this.clearValue = true;
    this.resetAllValues = false;
    this.customerTime = this.arrayOfCustomers[index];
    console.log(this.customerTime);
    
    // const customerName = this.arrayOfCustomers[index].customerName
    // console.log(customerName);
    
    // this.arrayOfCustomers.forEach((v, i) => {
    //   if (v.customerName == customerName) {
    //     this.arrayOfCustomerSelected.push(v);
    //     console.log(this.arrayOfCustomerSelected);
        
    //     this.arrayOfCustomers.splice(i, 1);
    //     this.arrayOfCustomersName.splice(i, 1);
    //     this.clearValue = true;
    //     this.resetAllValues = false;
    //     console.log(v);

    //   }
    // });
    // this.clearValue = false;
    // const inde = this.arrayOfCustomerSelected.findIndex(x => x.customerName == index.customerName);
    // this.customerTime = this.arrayOfCustomerSelected[inde];

  }
  // for (var i = 0; i < this.arrayOfCustomerSelected.length; i++) {
    //   if (this.arrayOfCustomerSelected[i].customerCode === index.customerCode) {
    //       this.obj = i;
         

    //   }
// }


  onSelectRepeatedValue(e) {
    this.repeatedValue = e.target.value;
    if (this.repeatedValue == "No Repeat") {
      this.dateArray = [];
      this.arrayTerritoryCustomerVisits = [];

    }
  }
  handleSpliceCustomerArray(event) {
    const index = this.arrayOfCustomerSelected.findIndex(x => x.customerCode == event.object.event.meta.customerCode);
    let arr = JSON.parse(JSON.stringify(this.territoryCustomerVisit.territoryCustomerVisits));
    const indexOfVisit = arr.findIndex(x => x.id.customerCode == event.object.event.meta.customerCode);
    this.territoryCustomerVisit.territoryCustomerVisits.splice(indexOfVisit, 1);
    this.arrayOfCustomerSelected.splice(index, 1);
    this.arrayOfCustomers.push(event.object.event.meta);
    this.arrayOfCustomersName.push(event.object.event.meta.customerName);
    this.arrayOfCustomers.forEach(cus => cus.customerCode == event.object.event.meta.customerCode ?
      cus.labal = "" : cus.labal = cus.labal);
  }
  ReciveDate(event) {
    this.dateJourny = event;
  }
  ReciveTime(event) {
    event.sort((a, b) => a.start - b.start);
    this.resetAllValues = false;
    let arr = [];
    let counter = 0;
    this.territoryCustomerVisit.companyCode = sessionStorage.getItem("companyCode");
    this.territoryCustomerVisit.territoryCode = this.territoryCode;
    this.territoryCustomerVisit.visitDate = this.dateJourny;
    event.forEach((v, i) => {
      this.arrayOfCustomers.forEach(cus => cus.customerCode == v.meta.customerCode ?
        cus.labal = (i + 1).toString() : cus.labal = cus.labal);
      if (v.meta.customerName) {
        counter++;
        arr.push({
          id: {
            companyCode: sessionStorage.getItem("companyCode"),
            territoryCode: this.territoryCode,
            customerCode: v.meta.customerCode,
            visitDate: this.dateJourny
          },
          endTime: moment(v.end).format("YYYY-MM-DDTHH:mm:ss"),
          startTime: moment(v.start).format("YYYY-MM-DDTHH:mm:ss"),
          visitOrder: i + 1
        });
      }
    });
    this.territoryCustomerVisit.territoryCustomerVisits = arr;
  }
  onReciveSelectedCustomer(obj) {
    this.resetAllValues = false;
    this.customerTime = obj;
  }
  onSelectDate() {
    if (this.from && this.to) {
      this.calaculateRepeated();
    }
  }
  onPressSubmit() {
    if (this.dateArray.length > 0) {
      this.saveArrayOfPlans();
    } else {
      this.planProvider.assignVistCustomer(this.territoryCustomerVisit).subscribe((Res) => {
        this.success = true;
        this.error = false;
        this.Responsemessage = "Success To Add Visit";
        setTimeout(() => {
          this.success = false;
          this.router.navigateByUrl("pages/plan/all-plans");
        }, 3000);
      }, error => {
        this.error = true;
        this.success = false;
        this.Responsemessage = error.error.message;
      });
    }
  }
  onPressCancel() {
    this.router.navigateByUrl("/pages/plan/all-plans");
  }

  onPressOk() {
    if (this.territoryCode && this.repeatedValue == "No Repeat") {
      this.pressOK = true;
      this.error = false;
    } else if (!this.territoryCode && this.repeatedValue == "No Repeat") {
      this.error = true;
      this.Responsemessage = "Please Enter Your Territory You Want To Add !";
    } else if (this.territoryCode && this.repeatedValue != "No Repeat" && this.dateArray.length > 0) {
      this.pressOK = true;
      this.error = false;
    } else if (this.territoryCode && this.repeatedValue != "No Repeat" && this.dateArray.length == 0) {
      this.error = true;
      this.Responsemessage = "Make Sure The To (Date) Greater Than From (Date)";
    }
  }
  calaculateRepeated() {
    const datesArr = [];
    let currentDate = moment(this.from);
    const stopDate = moment(this.to);
    if (this.repeatedValue === "Daily") {
      while (currentDate <= stopDate) {
        datesArr.push(moment(currentDate).format("YYYY-MM-DD"));
        currentDate = moment(currentDate).add(1, "days");
      }
    } else if (this.repeatedValue === "Monthly") {
      const dateStart = moment(this.from);
      const dateEnd = moment(this.to);
      while (
        dateEnd > dateStart ||
        dateStart.format("M-D") === dateEnd.format("M-D")
      ) {
        datesArr.push(dateStart.format("YYYY-MM-DD"));
        dateStart.add(1, "month");
      }
    } else if (this.repeatedValue === "Weekly") {
      const start = moment(this.from);
      const end = moment(this.to).add("d");
      const daysWillRepeate = start.day();
      const tmp = start.clone().day(daysWillRepeate);
      if (tmp.isAfter(start, "d")) {
        datesArr.push(tmp.format("YYYY-MM-DD"));
      }
      while (tmp.isBefore(end)) {
        tmp.add(7, "days");
        datesArr.push(tmp.format("YYYY-MM-DD"));
      }
      // datesArr.splice(this.dateArray.length - 1, 1);
    }
    this.dateArray = datesArr;
    // this.saveArrayOfDates();
  }
  saveArrayOfDates(e) {
    this.arrayOfCustomers.forEach(cus => cus.labal = "");
    this.arrayTerritoryCustomerVisits = [];
    this.dateArray.forEach(element => {
      this.arrayTerritoryCustomerVisits.push({
        companyCode: sessionStorage.getItem("companyCode"),
        territoryCode: this.territoryCode,
        visitDate: element,
        territoryCustomerVisits: [],
      });
    });

    this.arrayTerritoryCustomerVisits.forEach((elem, inde) => {
      e.forEach((v, i) => {
        this.arrayTerritoryCustomerVisits[inde].territoryCustomerVisits.push({
          id: {
            companyCode: sessionStorage.getItem("companyCode"),
            territoryCode: this.territoryCode,
            customerCode: v.meta.customerCode,
            visitDate: elem.visitDate,
          },
          visitOrder: 0,
          endTime: moment(v.end).format("YYYY-MM-DD") == elem.visitDate ? moment(v.end).format("YYYY-MM-DDTHH:mm:ss") : null,
          startTime: moment(v.start).format("YYYY-MM-DD") == elem.visitDate ? moment(v.start).format("YYYY-MM-DDTHH:mm:ss") : null
        });
      });
    });
    this.arrayTerritoryCustomerVisits.forEach(v => {
      v.territoryCustomerVisits = v.territoryCustomerVisits.filter(obj => obj.startTime != null);
      v.territoryCustomerVisits.sort((a, b) => a.start - b.start);
      v.territoryCustomerVisits.forEach((element, index) => element.visitOrder = index + 1);
      v.territoryCustomerVisits.forEach((ele, i) => {
        this.arrayOfCustomers.forEach(cus => cus.customerCode == ele.id.customerCode ?
          cus.labal = (ele.visitOrder).toString() : cus.labal = cus.labal);
      });
    });
    console.log(this.arrayTerritoryCustomerVisits);
    // this.saveArrayOfPlans();


  }

  onPressTr(code) {
    this.router.navigate(["/pages/customer/view-customer", code]);
  }
  saveArrayOfPlans() {
    this.planProvider
      .assignArrayOfVistCustomer(this.arrayTerritoryCustomerVisits)
      .subscribe(
        Res => {
          this.success = true;
          this.Responsemessage = "Succesfully Added !";
          setTimeout(() => {
            this.router.navigateByUrl("/pages/plan/all-plans");
          }, 2000);
        },
        error => {
          this.error = true;
          this.success = false;
          this.Responsemessage = error.error.message;
        }
      );
  }
  onPressToRestValue() {
    this.resetAllValues = true;
    this.arrayTerritoryCustomerVisits = [];
    this.territoryCustomerVisit = {
      companyCode: "",
      territoryCode: "",
      visitDate: "",
      territoryCustomerVisits: []
    };
    this.territoryCode = null;
    this.arrayOfCustomers = [];
    this.arrayOfCustomersName = [];
    this.arrayOfCustomerSelected = [];
    this.success = false;
    this.error = false;
    this.Responsemessage = "";
    this.repeatedValue = "No Repeat";
    this.from = null;
    this.to = null;
    this.dateArray = [];
    this.pressOK = false;
  }

}





