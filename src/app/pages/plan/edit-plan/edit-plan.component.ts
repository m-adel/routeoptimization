import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PlanService } from '../service/plan.service';
import { TerritoryCustomerVisits } from '../interface/territory-customer-visits';
import { Observable } from "rxjs";
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap } from "rxjs/operators";
import * as moment from 'moment';

@Component({
  selector: 'app-edit-plan',
  templateUrl: './edit-plan.component.html',
  styleUrls: ['./edit-plan.component.sass']
})
export class EditPlanComponent implements OnInit {
  date: any;
  from: any;
  to: any;

  territoryLatLng: { lat: any; lng: any; };
  territoryCode: any;
  searching = false;
  searchFailed = false;
  searchFrom = "";
  territoryCustomerVisit: TerritoryCustomerVisits = { territoryCustomerVisits: [] } as TerritoryCustomerVisits;
  arrayOfCustomers = [];
  arrayOfCustomersName = [];
  arrayOfCustomerSelected = [];
  showArrayForEditing = [];
  clearValue = false;
  dragableSwepper: any;
  dateJourny: any;
  success = false;
  error = false;
  Responsemessage = "";
  territoryMapName: any;
  weeklyTerritory: any;
  territoryName: any;
  pressOK = true;
  customerTime: any;
  resetAllValues = false;
  allCustomers = [];
  constructor(public router: Router, private planProvider: PlanService, private activeRoute: ActivatedRoute) { }
  formatter = (result: string) => result.toUpperCase();
  search = (text$: Observable<string>) =>
    text$.pipe(debounceTime(300), distinctUntilChanged(), tap(() => (this.searching = true)),
      switchMap(term => this.planProvider.searchByName(term, this.searchFrom).pipe(tap(),
        catchError(() => {
          this.searchFailed = true; return [];
        }))), tap(() => (this.searching = false)))

  ngOnInit() {
    this.date = this.activeRoute.snapshot.params.date;
    this.territoryCode = this.activeRoute.snapshot.params.id;
    this.getCustomerCodes();
    this.getTerritoryDetails();
  }

  getCustomerCodes() {
    this.planProvider.getCurrentCustomersVistsByDate(this.date, this.territoryCode).subscribe((Res: any) => {
      this.arrayOfCustomerSelected = Res;
      console.log(Res);
      this.arrayOfCustomerSelected.forEach(v => {
        const endDa = new Date(v.endTime);
        const StartDa = new Date(v.startTime);
        endDa.setTime(endDa.getTime() + endDa.getTimezoneOffset() * 60 * 1000);
        StartDa.setTime(StartDa.getTime() + StartDa.getTimezoneOffset() * 60 * 1000);
        v.startTime = moment(StartDa).format("HH:mm");
        v.endTime = moment(endDa).format("HH:mm");

      });
      this.getAllCustomerOfTerritory();
    });
  }

  onReciveSelectedCustomer(obj) {

    const customerName = obj.customerName.toUpperCase();
    this.arrayOfCustomers.forEach((v, i) => {
      if (v.customerName.toUpperCase() == customerName) {
        this.arrayOfCustomerSelected.push(v);
        this.arrayOfCustomersName.splice(i, 1);
        this.clearValue = true;
        this.resetAllValues = false;
        console.log(this.arrayOfCustomerSelected);

      }
    });
    this.clearValue = false;
    const inde = this.arrayOfCustomerSelected.findIndex(x => x.customerName == obj.customerName);
    this.customerTime = this.arrayOfCustomerSelected[inde];
    console.log(this.customerTime);

    this.mapCustomer();

  }

  getTerritoryDetails() {
    this.planProvider.getCurrentTerritoryForEdit(this.territoryCode).subscribe((Res: any) => {
      this.territoryName = Res.territory.name1;
      this.territoryLatLng = { lat: Res.territory.latitude, lng: Res.territory.longitude };
      console.log(this.territoryLatLng);
    });
  }
  SearchFrom(e) {
    this.searchFrom = e === "searchTerritory" ? "searchTerritory" : "searchTask";
  }
  getAllCustomerOfTerritory() {
    this.planProvider.getAllCustomerForSingleTerritory(this.territoryCode).subscribe((Res: any) => {
      this.arrayOfCustomers = [];
      this.allCustomers = Res;
      const arrCustomer = [];
      Res.forEach((el, i) => {
        arrCustomer.push({
          customerName: el.customerName,
          endTime: "", startTime: "", visitOrder: i + 1,
          latitude: el.latitude, longitude: el.longitude,
          id: {
            companyCode: el.companyCode, customerCode: el.customerCode,
            territoryCode: this.territoryCode
          }
        });
        this.arrayOfCustomerSelected.forEach((v, inde) => {
          if (v.id.customerCode === el.customerCode) {
            v.customerName = el.customerName;
            v.latitude = el.latitude;
            v.longitude = el.longitude;
            v.label = v.visitOrder.toString();
          }
        });
      });
      this.arrayOfCustomers =
        arrCustomer.filter((v) =>
          this.arrayOfCustomerSelected.findIndex(x => x.customerName == v.customerName) === -1);
      this.arrayOfCustomersName = this.arrayOfCustomers.map(v => v.customerName);
      this.showArrayForEditing = JSON.parse(JSON.stringify(this.arrayOfCustomerSelected));
      console.log(this.arrayOfCustomers);

      this.mapCustomer();

    });

  }
  ReciveTime(event) {
    event.sort((a, b) => a.start - b.start);
    this.resetAllValues = false;
    const arr = [];
    let counter = 0;
    this.territoryCustomerVisit.companyCode = sessionStorage.getItem("companyCode");
    this.territoryCustomerVisit.territoryCode = this.territoryCode;
    this.territoryCustomerVisit.visitDate = this.dateJourny;
    event.forEach((v, i) => {
      this.mapCustomer();
      if (v.meta.customerName) {
        counter++;
        arr.push({
          id: {
            companyCode: sessionStorage.getItem("companyCode"),
            territoryCode: this.territoryCode,
            customerCode: v.meta.customerCode || v.meta.id.customerCode, visitDate: this.dateJourny
          },
          endTime: moment(v.end).format("YYYY-MM-DDTHH:mm:ss"),
          startTime: moment(v.start).format("YYYY-MM-DDTHH:mm:ss"),
          visitOrder: i + 1
        });
      }
    });
    this.territoryCustomerVisit.territoryCustomerVisits = arr;
    this.mapCustomer();

  }
  ResiveCustomer(event) {
    const customerName = event.selectedData.toUpperCase();
    this.arrayOfCustomers.forEach((v, i) => {
      if (v.customerName.toUpperCase() == customerName) {
        this.arrayOfCustomerSelected.push(v);
        // this.arrayOfCustomers.splice(i, 1);
        this.arrayOfCustomersName.splice(i, 1);
        this.clearValue = true;
        this.resetAllValues = false;
      }
    });
    this.clearValue = false;
    const inde = this.arrayOfCustomerSelected.findIndex(x => x.customerName == event.selectedData);
    this.customerTime = this.arrayOfCustomerSelected[inde];
    this.mapCustomer();
  }
  handleSpliceCustomerArray(event) {
    const index = this.arrayOfCustomerSelected.findIndex(x => x.customerCode == event.object.event.meta.customerCode);
    let arr = JSON.parse(JSON.stringify(this.territoryCustomerVisit.territoryCustomerVisits));
    const indexOfVisit = arr.findIndex(x => x.id.customerCode == event.object.event.meta.customerCode);
    this.territoryCustomerVisit.territoryCustomerVisits.splice(indexOfVisit, 1);
    this.arrayOfCustomerSelected.splice(index, 1);
    this.showArrayForEditing.splice(index, 1);
    this.arrayOfCustomers.push(event.object.event.meta);
    this.arrayOfCustomersName.push(event.object.event.meta.customerName);
    this.mapCustomer();
    this.arrayOfCustomerSelected.forEach(cus => cus.customerCode == event.object.event.meta.customerCode ?
      cus.labal = "" : cus.labal = cus.labal);

  }
  dragStart(event, i) {
    this.dragableSwepper = i;
  }
  allowDrop(event) {
    event.preventDefault();
  }
  drop(event, index) {
    event.preventDefault();
    const element = this.arrayOfCustomerSelected[index];
    this.arrayOfCustomerSelected[index] = this.arrayOfCustomerSelected[this.dragableSwepper];
    this.arrayOfCustomerSelected[this.dragableSwepper] = element;
  }
  onChangStartOrEnd(event, code, from) {
    const value = event.target.value;
    this.arrayOfCustomerSelected.forEach((v, i) => {
      const ele = document.getElementById(v.customerCode) as HTMLInputElement;
      if (v.id.customerCode == code) {
        if (from === "start") {
          v.startTime = value;
        } else {
          v.endTime = value;
        }
      }
    });
  }
  ReciveDate(e) {
    this.dateJourny = e;

  }
  handleSubmitAddJournyPlan() {
    console.log(this.territoryCustomerVisit);
    this.onSavePlan();
  }
  onSavePlan() {
    this.planProvider.assignVistCustomer(this.territoryCustomerVisit).subscribe((Res) => {
      this.success = true;
      this.error = false;
      this.Responsemessage = "Success To Add Visit";
      setTimeout(() => {
        this.success = false;
        this.router.navigateByUrl("pages/plan/all-plans");
      }, 3000);
    }, error => {
      this.error = true;
      this.success = false;
      this.Responsemessage = error.error.message;
    });
  }
  onPressCancel() {
    this.router.navigateByUrl("/pages/plan/all-plans");
  }
  onPressToRestValue() {
    this.resetAllValues = true;
    this.territoryCustomerVisit = { companyCode: "", territoryCode: "", visitDate: "", territoryCustomerVisits: [] };
    this.territoryCode = null;
    this.arrayOfCustomers = [];
    this.arrayOfCustomersName = [];
    this.arrayOfCustomerSelected = [];
    this.success = false;
    this.error = false;
    this.Responsemessage = "";
  }
  mapCustomer() {
    // console.log(this.territoryCustomerVisit.territoryCustomerVisits);
    this.allCustomers.forEach(cus => cus.labal = "");

    const clonedArray = JSON.parse(JSON.stringify(this.territoryCustomerVisit.territoryCustomerVisits))
    if (clonedArray.length) {
      this.allCustomers.forEach(cus => {
        clonedArray.forEach(ele => {
          if (cus.customerCode === ele.id.customerCode) {
            cus.labal = ele.visitOrder.toString();
          }
        });
      });
    } else {
      this.allCustomers.forEach(cus => {
        this.arrayOfCustomerSelected.forEach(ele => {
          if (cus.customerCode === ele.id.customerCode) {
            cus.labal = ele.visitOrder.toString();
          }
        });
      });
    }
  }
}



