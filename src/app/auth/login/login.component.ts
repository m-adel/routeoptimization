import { HandleErrorService } from '../../providers/handle-error.service';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  Responsemessage: string;
  name = "";
  password = "";
  success = false;
  error = false;
  constructor(
    public router: Router,
    private loginService: LoginService,
    private errorHandler: HandleErrorService
  ) { }

  ngOnInit() {}

  handelPressLogin() {
    const dataSendToLogin = {
      username: this.name,
      password: this.password
    };
    this.loginService.Login(dataSendToLogin).subscribe((Res: any) => {
      console.log(Res);
      this.success = true;
      this.error = false;
      this.Responsemessage = "Login Successfully !";
      console.log(Res);
      sessionStorage.setItem('accessToken', Res.accessToken);
      sessionStorage.setItem('userName', Res.user.userName);
      sessionStorage.setItem("userId", Res.user.userId);
      sessionStorage.setItem("profileCode", Res.user.profile.profileCode);
      sessionStorage.setItem("profileId", Res.user.profile.profileId);
      this.getCompanyCode(Res.user.userId);
      setTimeout( () => {
        this.success = false;
        this.router.navigateByUrl("/pages/user/all-users");
      }, 2000);
    }, err => {
      this.Responsemessage = this.errorHandler.errorHandler(err.status, "login");
      this.error = true;
    });
  }
  getCompanyCode(userId) {
    this.loginService.getAssiendCompany(userId).subscribe((Resp: any) => {
      console.log(Resp[0]);
      if (Resp[0].company.companyCode) {
        sessionStorage.setItem("companyCode", Resp[0].company.companyCode);
        sessionStorage.setItem("companyName", Resp[0].company.companyName);
      }
    });
  }

}
