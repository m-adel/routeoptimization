import { WebServicesService } from '../../providers/web-services.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class LoginService {
  constructor( public http: HttpClient, private config: WebServicesService) { }

  Login(loginData) {
    const url = this.config.serviceUrl + this.config.authenticationLoginURL;
    return this.http.post(url, loginData);
  }
  getAssiendCompany(id) {
    const url = this.config.serviceUrl + this.config.getAssiedndCompanyByUserId.replace("{0}", id);
    this.config.accessToken = sessionStorage.getItem('accessToken')
    this.config.updateHeaders()
    const header = this.config.headers;
    return this.http.get(url, {headers: header} );
  }
}
