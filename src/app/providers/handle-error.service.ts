import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class HandleErrorService {
  message: any;
  constructor(public router: Router) { }
  errorHandler(status, from) {
    this.message = "";
    if (from === "login") {
      switch (status) {
        case 401: this.message = "UserName Or Password is Not correct"; break;
        case 0: this.message = "he server is temporarily unable Try again Later"; break;
        default: break;
      }
    } else if (from === "pages") {
      switch (status) {
        case 0:
        this.message = "You Must Login Before Calling This Service";
        setTimeout( () =>  this.router.navigate(["/login"]), 2000);
        break;
        case 503: this.message = "he server is temporarily unable Try again Later"; break;
        default: break;
      }
    }
    return this.message;
  }
}
