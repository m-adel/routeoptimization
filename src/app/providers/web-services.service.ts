import { HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class WebServicesService {
  constructor() {}
  accessToken = sessionStorage.getItem("accessToken");
  headers = new HttpHeaders({
    Authorization: "Bearer" + " " + this.accessToken
  });
  authenticationLoginURL = "authentication/login";
  // serviceUrl = 'http://192.168.1.100:8088/VEyeKWT/rest/';
  // serviceUrl = "http://137.117.167.68/VEyeWS/rest/";
  serviceUrl = "http://137.117.167.68/VEyeRoute/rest/";

  // serviceDashboard = "http://137.117.167.68:8080/api/statistics/dashboard/";
  serviceDashboard = "http://137.117.167.68:8070/api/statistics/dashboard/";

  getAssiendCompaniesByUserId =
    "userCompanies?id.userId={0}&limit=999&sort=company.companyName";
  getAssiedndCompanyByUserId = "userCompanies?id.userId={0}";
  getCountOfUsers = "users/count?userCompanies.id.companyCode=in:{2}";
  allUsers = "users";
  getAllUserInCompany =
    "users?userCompanies.id.companyCode=in:{0}&offset={1}&limit={2}";
  getAllUsersInCompanyTerritory =
  "users?userCompanies.id.companyCode=in:{0}&offset={1}&limit={2}&profile.profileId={3}";
  searchByUserNameInCompany =
    "users?userName=like:{0}&userCompanies.id.companyCode={1}&limit={2}&offset={3}";
  createNewUser = "users?companyCode={companyCode}";
  deleteUser = "users/{userCode}";
  getProfiles = "profiles";
  viewProfile = "users/{userId}";
  getCountOfMaterial =
    "materials/count?materialSalesData.id.companyCode=in:{companyCode}";
  getAllProductForCompany =
    "materials?materialSalesData.id.companyCode=in:{0}&offset={1}&limit={2}";
  searchByMaterialName =
    "materials?&name1=like:{0}&materialSalesData.id.companyCode=in:{1}";
  materialLookUp = "materials/lookups";
  uploadFile = "files/upload";
  addMaterial = "materials?companyCode={companyCode}";
  getAllCountOfMaterial = "materials/count";
  getAllMaterial = "materials?offset={lastCode}&limit=1";
  deleteMaterial = "materials/{materialCode}";
  showMaterial = "materials/{materialCode}";
  updateMaterial = "materials";
  getCountOfTaskType = "taskTypes/count";
  getAllTaskType = "taskTypes?offset={1}&limit={2}";
  searchTaskTypeName = "taskTypes?description=like:{0}&offset={1}";
  addTaskType = "taskTypes";
  deleteTaskType = "taskTypes/{id}";
  editTaskType = "taskTypes";
  getCountOfTasks = "tasks/count?&taskCompanies.id.companyCode={0}";
  getTasks = "tasks?taskCompanies.id.companyCode={0}&offset={1}&limit={2}";
  searchByTaskName =
    "tasks?name=like:{0}&taskCompanies.id.companyCode={1}&offset={2}";
  deleteTask = "tasks/{taskId}";
  addTask = "taskVersions?companyCode={companyCode}";
  getTaskAttachments = "attachmentTypes";
  getActionType = "actionTypes";
  getScoreOperators = "scoreOperators?actionTypes.id={id}";
  addTaskAttachment = "taskAttachments";
  getTaskAttachmentForEdit =
    "taskAttachments?taskId={0}&taskVersion={1}&limit=99999999";
  addMaterialsToTask = "taskRestrictedMaterials/assignments";
  EditMaterialTask = "taskRestrictedMaterials";
  deleteProductFromTask = "taskRestrictedMaterials/{id}";
  getTaskVersion = "taskVersions?task.id={taskId}";
  addTaskVersion = "taskVersions?companyCode={companyCode}";
  releaseTask = "tasks/{id}/releases";
  getProductsForSingleTask =
    "taskRestrictedMaterials/detailed?taskId={0}&taskVersion={1}&offset={2}&limit={3}";
  countOfRootForSingleCompany =
    "territories/count?territorySalesData.id.companyCode=in:{companyCode}";
  getAllRootsForCompony =
    "territories?territorySalesData.id.companyCode=in:{0}&offset={1}&limit={2}";
  searchByRootName =
    "territories?name1=like:{1}&territorySalesData.id.companyCode=in:{2}&offset={3}&limit={4}";
  deleteRoot = "territories/{territoryCode}";
  getAllCountries = "territories/lookups";
  getAllRegions = "regions";
  getAllGovernoment = "governorates";
  addTerritory = "territories?companyCode={companyCode}";
  addTerritoryDataSales = "territorySalesData";
  addUserTerritories = "userTerritories";
  searchOfCustomerForTerritory =
    "customers?name1=like:{0}&customerSalesData.id.companyCode=in:{1}";
  AssignCustomerTerritory = "territoryCustomers/assignments";
  getCuurentTerritory =
    "territorySalesData/findUsingMatrixParam;territoryCode={0};companyCode={1}";
  editTerritory = "territories";
  currentUserTerritory = "userTerritories?territory.territoryCode=like:{0}";
  currentCustomerTerritory =
    "territoryCustomers?id.companyCode={0}&id.territoryCode={1}";
  getCustomerNamesAssignedToRoot =
    "customers?customerCode={0}&customerSalesData.id.companyCode=in:{1}";
  unassignCustomerTerritory =
    "territoryCustomers/unassignments?companyCode={0}&territoryCode={1}&customerCode={2}";
  getCustomerVistisByDate =
    "territoryCustomerVisits?id.companyCode={0}&id.visitDate={1}&limit={2}&sort=visitOrder";
  getCustomerVistis = "territoryCustomerVisits?id.companyCode={0}&limit=1000000000";
//    "territoryCustomerVisits/{from}/{to}?id.companyCode={0}&id.territoryCode={3}&id.visitDate={1}&limit={2}&sort=visitOrder";

testTerritory = "territoryCustomerVisits?id.companyCode=COMP1&id.territoryCode=0000100103"

  getCurrentCustomerVistisByDate =
    "territoryCustomerVisits?id.companyCode={0}&id.territoryCode={3}&id.visitDate={1}&limit={2}&sort=visitOrder";
  searchTerritory =
    "territories?name1=like:{0}&active=1&territorySalesData.id.companyCode=in:{1}";
  GetAllCustomerForTerritory =
    "territoryCustomers/locations?companyCode={0}&territoryCode={1}";
  assignCustomerVisit = "territoryCustomerVisits/assignments";
  assignTaskCustomer = "taskCustomers/assignments";
  allCustomerCountForCompany =
    "customers/count?customerSalesData.id.companyCode=in:{companyCode}";
  allCustomerCount = "customers/count";
  getAllCustomers =
    "customers?customerSalesData.id.companyCode=in:{0}&offset={1}&limit={2}";
  searchByCustomerName =
    "customers?name1=like:{1}&customerSalesData.id.companyCode=in:{2}&offset=0&limit={3}";
  getCustomerLookUps = "customers/lookups";
  getCutomerTypeLookUps = "customerSalesData/lookups";
  getAttachmentType = "attachmentTypes";
  addCustomer = "customers?companyCode={companyCode}";
  addCustomerContact = "customerContacts";
  addCustomerDataSales = "customerSalesData";
  addCustomerAttach = "customerAttachments";
  addCutomerAssets = "customerAssets";
  deleteCustomer = "customers/{customerCode}";
  customerDetails =
    "customerSalesData/detail;customerCode={customerCode};companyCode={companyCode}";
  customerAttachDetails = "customers/{customerCode}/detail";
  editCustomer = "customers";
  getCountOfTaskCustomer = "taskCustomers/count?taskCompany.id.companyCode={0}";
  getTaskCustomer =
    "taskCustomers?taskCompany.id.companyCode={1}&offset={2}&limit={3}";
  searchByTaskCustomerName =
    "taskCustomers?taskCompany.id.companyCode={0}&taskCompany.task.name=like:{1}&offset=0&limit=1000000";
  deleteTaskCustomer = "taskCustomers/{id}";
  currentTaskCustomer =
    "taskCustomers?taskCompany.id.companyCode={1}&offset=0&limit={2}&validFrom={3}&validTo={4}&taskCompany.id.taskId={5}";
  countOfTaskCurrentCustomer =
    "taskCustomers/count?taskCompany.id.taskId={0}&taskCompany.id.companyCode={1}&validFrom={2}&validTo={3}";
  deleteCustomerAssets = "customerAssets/{assetId}";
  deleteCustomerAttachment = 'customerAttachments/{attachmentId}';
  deleteCustomerContact = "customerContacts/{contactId}";
  getTotalStatsics = 'statistics/countAll';
  getAllCustomersLocations = "customerLatLngs";
  getLogs = "wsLogs";
  customerDashBoard = "customer";
  customerDashboardForSelectedTaskWithData = "customerWithSelectedTaskAndDate/{taskId}/{validFrom}/{validTo}";
  actionDashboardForSelectedTaskWithData = "actionsWithTaskAndDate/{taskId}/{validFrom}/{validTo}";
  taskDashboardForSelectedTaskWithData = "tasksWithTaskIdAndDate/{taskId}/{validFrom}/{validTo}";
  customerLatLngWithDateAndTaskId = "customerLatLngsWithTaskIdAndDate/{taskId}/{validFrom}/{validTo}";
  productInTaskWithDate = "productsInTaskWithDate/{taskId}/{validFrom}/{validTo}";
  taskActionsOfProducts = "getTaskDetailsForProduct/{productCode}/{taskId}/{vaildFrom}/{validTo}";
  taskActionsOfAllProducts = "getTaskDetailsForAllProduct/{taskId}/{vaildFrom}/{validTo}";
  CharttaskActionsOfProducts = "chartOfTaskOptions/{actionId}/{taskId}/{validFrom}/{validTo}";
  taskDashboard = "tasks";
  visitDashboard = "visits";
  actionsDashboard = "actions";
  updateHeaders() {
    this.headers = new HttpHeaders({
      Authorization: "Bearer" + " " + this.accessToken
    });
  }
}
