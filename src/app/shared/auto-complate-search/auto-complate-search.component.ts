import { Component, EventEmitter, Input, OnInit, Output, OnChanges, ViewChild } from '@angular/core';
import { distinctUntilChanged, filter } from 'rxjs/operators';
import { debounceTime, map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { Subject, merge } from 'rxjs';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-auto-complate-search',
  templateUrl: './auto-complate-search.component.html',
  styleUrls: ['./auto-complate-search.component.sass']
})
export class AutoComplateSearchComponent {
  @Input() dataSource: any;
  @Input() from: any;
  @Input() name: any;
  @Output() selectedData = new EventEmitter();
  @Input() placeholderValue: any;
  @Input() readOnly: any;


  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  nameSearch: string;
  formatter = (result: string) => result.toUpperCase();


        search = (text$: Observable<string>) => {
          const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
          const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
          const inputFocus$ = this.focus$;

          return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
            map(term => (term === '' ? this.dataSource
              : this.dataSource.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
          );
        }
  constructor() { }

  onChange(e) {
    const data = { DataFrom: this.from, selectedData: e.item };
    this.selectedData.emit(data);
    if (this.from == "plan") {
      setTimeout(() => {
        this.nameSearch = "";
      }, 200);
    }
  }
}
