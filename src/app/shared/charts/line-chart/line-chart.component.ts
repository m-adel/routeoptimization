import { Component, OnInit, ViewChild, Input, OnChanges, DoCheck } from "@angular/core";
import { Chart } from "angular-highcharts";
import * as moment from 'moment';


@Component({
  selector: "app-line-chart",
  templateUrl: "./line-chart.component.html",
  styleUrls: ["./line-chart.component.scss"]
})
export class LineChartComponent implements OnInit, DoCheck {
  chart: Chart;
  dateArray = [];
  @Input() data: any;
  @Input() date: any;
  flag = false;
  someRange = [0, 6];
  ngOnInit() {
    this.getDaysInCurrentMonth();
    this.init();
  }
  ngDoCheck() {
    if (this.data[0].data && this.data[0].data.length > 0 && this.flag === false) {
      this.init();
      this.flag = true;
    }

  }
  onChange() {
    this.getDaysInCurrentMonth();
    this.init();
  }

  init() {
    const chart = new Chart({
      chart: {
        type: "line"
      },
      title: {
        text: ""
      },
      credits: {
        enabled: false
      },
      series: this.data,
      xAxis: {
        categories: this.dateArray,
        min: this.someRange[0],
        max: this.someRange[1],
      tickLength: 0
     },
    });
    this.chart = chart;
    chart.ref$.subscribe(console.log);
  }
  getDaysInCurrentMonth() {
    this.dateArray = [];
    let startOfMonth = moment().startOf('month');
    const endOfMonth   = moment().endOf('month');
    while (startOfMonth <= endOfMonth) {
      this.dateArray.push( moment(startOfMonth).format('YYYY-MM-DD') );
      startOfMonth = moment(startOfMonth).add(1, 'days');
    }
    if (this.date && this.date.length > 0) {
      this.dateArray = this.date;
    }
  }
}
