import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-total-card',
  templateUrl: './total-card.component.html',
  styleUrls: ['./total-card.component.sass']
})
export class TotalCardComponent implements OnInit {
  total: any = { customerCount: null, userCount: null, productCount: null, taskCount: null, projects: null, plans: null };
  constructor(public router: Router) { }

  ngOnInit() {

  }
  onPressCustomer() {
    this.router.navigateByUrl("pages/customer/all-customers");
  }
  onPressUser() {
    this.router.navigateByUrl("/pages/user/all-users");
  }
  onPressTerritory() {
    this.router.navigateByUrl("/pages/product/all-products");
  }
  onPressTasks() {
    this.router.navigateByUrl("/pages/task/all-tasks");
  }
  onPressPlan() {
    this.router.navigateByUrl("/pages/plan/all-plans");
  }
  onPressProject() {
    this.router.navigateByUrl("/pages/project/all-projects");
  }

}
