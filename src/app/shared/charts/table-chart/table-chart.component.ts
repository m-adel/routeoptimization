import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table-chart',
  templateUrl: './table-chart.component.html',
  styleUrls: ['./table-chart.component.sass']
})
export class TableChartComponent implements OnInit {
  @Input() dataSource: any;
  constructor() { }

  ngOnInit() {
  }

}
