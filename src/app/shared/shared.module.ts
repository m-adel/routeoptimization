import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChartsModule } from "ng2-charts";
import { ChartModule } from "angular-highcharts";

import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ChildNavsComponent } from "./child-navs/child-navs.component";
import { LoaderComponent } from "./loader/loader.component";
import { AutoComplateSearchComponent } from "./auto-complate-search/auto-complate-search.component";
import { FormsModule } from "@angular/forms";
import { LineChartComponent } from "./charts/line-chart/line-chart.component";
import { TotalCardComponent } from "./charts/total-card/total-card.component";
import { PieChartComponent } from "./charts/pie-chart/pie-chart.component";
import { TableChartComponent } from "./charts/table-chart/table-chart.component";
import { LogsComponent } from "./charts/logs/logs.component";
import { NouisliderModule } from "ng2-nouislider";
import { BarChartComponent } from "./charts/bar-chart/bar-chart.component";

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    ChildNavsComponent,
    LoaderComponent,
    AutoComplateSearchComponent,
    LineChartComponent,
    TotalCardComponent,
    PieChartComponent,
    TableChartComponent,
    LogsComponent,
    BarChartComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,
    FormsModule,
    ChartsModule,
    ChartModule,
    NouisliderModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    NgbModule,
    ChildNavsComponent,
    LoaderComponent,
    AutoComplateSearchComponent,
    ChartsModule,
    LineChartComponent,
    TotalCardComponent,
    PieChartComponent,
    ChartModule,
    TableChartComponent,
    LogsComponent,
    NouisliderModule,
    BarChartComponent
  ]
})
export class SharedModule {}
