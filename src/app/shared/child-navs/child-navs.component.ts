import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-child-navs',
  templateUrl: './child-navs.component.html',
  styleUrls: ['./child-navs.component.sass']
})
export class ChildNavsComponent implements OnInit {
@Input() navs: any;
companyName: any;
viewProfileDetails
  constructor(public router: Router) { }

  ngOnInit() {
      this.companyName = sessionStorage.getItem("companyName");
  }
  handleViewProfile() {
    this.viewProfileDetails = false;
  }
}
