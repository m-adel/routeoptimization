import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass'],
})
export class HeaderComponent implements OnInit {
  viewProfileDetails = false;
  userName = "";
  constructor(public router: Router) { }
  ngOnInit() {
    // tslint:disable-next-line:no-unused-expression
    sessionStorage.getItem("userName") ? this.userName = sessionStorage.getItem("userName") : '';
  }
  onClickImageProfile() {
    this.viewProfileDetails = !this.viewProfileDetails;
  }
  handleViewProfile() {
    this.viewProfileDetails = false;
  }
  handleLogOut() {
    sessionStorage.clear();
    this.router.navigateByUrl("/login");
  }


}
